package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.ToggleButton;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class app_sons extends Activity {
    GlobalMethods globalMethods;

    Intent intent;

    public static SeekBar sbVolume;
    float volume = 1;

    public static Boolean bPlaying = false;

    ToggleButton btnAmbientes1;
    ToggleButton btnAmbientes2;
    ToggleButton btnAmbientes3;
    ToggleButton btnAmbientes4;
    ToggleButton btnAmbientes5;
    ToggleButton btnAmbientes6;
    ToggleButton btnAmbientes7;
    ToggleButton btnAmbientes8;

    ToggleButton btnAnimais1;
    ToggleButton btnAnimais2;
    ToggleButton btnAnimais3;
    ToggleButton btnAnimais4;
    ToggleButton btnAnimais5;
    ToggleButton btnAnimais6;
    ToggleButton btnAnimais7;
    ToggleButton btnAnimais8;
    ToggleButton btnAnimais9;
    ToggleButton btnAnimais10;

    ToggleButton btnInstrumentos1;
    ToggleButton btnInstrumentos2;
    ToggleButton btnInstrumentos3;
    ToggleButton btnInstrumentos4;
    ToggleButton btnInstrumentos5;

    ToggleButton btnTempo1;
    ToggleButton btnTempo2;
    ToggleButton btnTempo3;

    MediaPlayer ambientes_floresta_dia;
    MediaPlayer ambientes_floresta_noite;
    MediaPlayer ambientes_cachoeira;
    MediaPlayer ambientes_caverna;
    MediaPlayer ambientes_fogueira;
    MediaPlayer ambientes_praia;
    MediaPlayer ambientes_rio;
    MediaPlayer ambientes_profundezas;

    MediaPlayer animais_baleia;
    MediaPlayer animais_lobo;
    MediaPlayer animais_grilo;
    MediaPlayer animais_coruja;
    MediaPlayer animais_ovelha;
    MediaPlayer animais_sapo;
    MediaPlayer animais_gato;
    MediaPlayer animais_cigarra;
    MediaPlayer animais_cavalo;
    MediaPlayer animais_pavao;

    MediaPlayer instrumentos_espanta_espiritos;
    MediaPlayer instrumentos_caixinha;
    MediaPlayer instrumentos_flauta;
    MediaPlayer instrumentos_violao;
    MediaPlayer instrumentos_sino_chines;

    MediaPlayer tempo_chuva;
    MediaPlayer tempo_trovao;
    MediaPlayer tempo_vento;

    LinearLayout llAmbientes2;
    LinearLayout llAmbientes2FREE;
    LinearLayout llAmbientes3;
    LinearLayout llAmbientes3FREE;
    LinearLayout llAmbientes4;
    LinearLayout llAmbientes4FREE;
    LinearLayout llAmbientes6;
    LinearLayout llAmbientes6FREE;
    LinearLayout llAnimais3;
    LinearLayout llAnimais3FREE;
    LinearLayout llAnimais4;
    LinearLayout llAnimais4FREE;
    LinearLayout llAnimais6;
    LinearLayout llAnimais6FREE;
    LinearLayout llAnimais7;
    LinearLayout llAnimais7FREE;
    LinearLayout llAnimais8;
    LinearLayout llAnimais8FREE;
    LinearLayout llIinstrumentos2;
    LinearLayout llIinstrumentos2FREE;
    LinearLayout llIinstrumentos3;
    LinearLayout llIinstrumentos3FREE;
    LinearLayout llIinstrumentos5;
    LinearLayout llIinstrumentos5FREE;
    LinearLayout llTempo1;
    LinearLayout llTempo1FREE;
    LinearLayout llTempo2;
    LinearLayout llTempo2FREE;

    //private LinearLayout layAdView;
    //AdView mAdView;

    //private FirebaseAnalytics firebaseAnalytics;
    //private Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSons));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSons));
        }

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_sons);

        globalMethods = new GlobalMethods(this);


        ImageView backSons = (ImageView) findViewById(br.com.relaxapp.R.id.backSons);
        backSons.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                voltar();
            }
        });


        sbVolume = (SeekBar) findViewById(R.id.sbVolumeSons);

        btnAmbientes1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes1);
        btnAmbientes2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes2);
        btnAmbientes3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes3);
        btnAmbientes4 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes4);
        btnAmbientes5 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes5);
        btnAmbientes6 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes6);
        btnAmbientes7 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes7);
        btnAmbientes8 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAmbientes8);

        btnAnimais1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais1);
        btnAnimais2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais2);
        btnAnimais3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais3);
        btnAnimais4 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais4);
        btnAnimais5 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais5);
        btnAnimais6 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais6);
        btnAnimais7 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais7);
        btnAnimais8 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais8);
        btnAnimais9 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais9);
        btnAnimais10 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnAnimais10);

        btnInstrumentos1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnInstrumentos1);
        btnInstrumentos2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnInstrumentos2);
        btnInstrumentos3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnInstrumentos3);
        btnInstrumentos4 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnInstrumentos4);
        btnInstrumentos5 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnInstrumentos5);

        btnTempo1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnTempo1);
        btnTempo2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnTempo2);
        btnTempo3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnTempo3);


        btnAmbientes1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(ambientes_floresta_dia == null) {
                    ambientes_floresta_dia = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_floresta_dia);
                    ambientes_floresta_dia.setLooping(true);
                }

                if(btnAmbientes1.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "ambientes_floresta_dia");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAmbientes1.setChecked(true);
                    ambientes_floresta_dia.start();
                    bPlaying = true;
                    btnAmbientes1.setAlpha(1);
                }else {
                    ambientes_floresta_dia.pause();
                    bPlaying = false;
                    btnAmbientes1.setAlpha(0.3f);
                }
            }
        });

        btnAmbientes5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(ambientes_fogueira == null) {
                    ambientes_fogueira = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_fogueira);
                    ambientes_fogueira.setLooping(true);
                }

                if(btnAmbientes5.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "ambientes_fogueira");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAmbientes5.setChecked(true);
                    ambientes_fogueira.start();
                    bPlaying = true;
                    btnAmbientes5.setAlpha(1);
                }else {
                    ambientes_fogueira.pause();
                    bPlaying = false;
                    btnAmbientes5.setAlpha(0.3f);
                }
            }
        });

        btnAmbientes8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(ambientes_profundezas == null) {
                    ambientes_profundezas = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_profundezas);
                    ambientes_profundezas.setLooping(true);
                }

                if(btnAmbientes8.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "ambientes_profundezas");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAmbientes8.setChecked(true);
                    ambientes_profundezas.start();
                    bPlaying = true;
                    btnAmbientes8.setAlpha(1);
                }else {
                    ambientes_profundezas.pause();
                    bPlaying = false;
                    btnAmbientes8.setAlpha(0.3f);
                }
            }
        });

        btnAnimais1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(animais_baleia == null) {
                    animais_baleia = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_baleia);
                    animais_baleia.setLooping(true);
                }

                if(btnAnimais1.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "animais_baleia");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAnimais1.setChecked(true);
                    animais_baleia.start();
                    bPlaying = true;
                    btnAnimais1.setAlpha(1);
                }else {
                    animais_baleia.pause();
                    bPlaying = false;
                    btnAnimais1.setAlpha(0.3f);
                }
            }
        });

        btnAnimais2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(animais_coruja == null) {
                    animais_coruja = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_coruja);
                    animais_coruja.setLooping(true);
                }

                if(btnAnimais2.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "animais_coruja");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAnimais2.setChecked(true);
                    animais_coruja.start();
                    bPlaying = true;
                    btnAnimais2.setAlpha(1);
                }else {
                    animais_coruja.pause();
                    bPlaying = false;
                    btnAnimais2.setAlpha(0.3f);
                }
            }
        });

        btnAnimais5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(animais_cigarra == null) {
                    animais_cigarra = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_cigarra);
                    animais_cigarra.setLooping(true);
                }

                if(btnAnimais5.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "animais_cigarra");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAnimais5.setChecked(true);
                    animais_cigarra.start();
                    bPlaying = true;
                    btnAnimais5.setAlpha(1);
                }else {
                    animais_cigarra.pause();
                    bPlaying = false;
                    btnAnimais5.setAlpha(0.3f);
                }
            }
        });

        btnAnimais9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(animais_cavalo == null) {
                    animais_cavalo = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_cavalo);
                    animais_cavalo.setLooping(true);
                }

                if(btnAnimais9.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "animais_cavalo");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAnimais9.setChecked(true);
                    animais_cavalo.start();
                    bPlaying = true;
                    btnAnimais9.setAlpha(1);
                }else {
                    animais_cavalo.pause();
                    bPlaying = false;
                    btnAnimais9.setAlpha(0.3f);
                }
            }
        });

        btnAnimais10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(animais_pavao == null) {
                    animais_pavao = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_pavao);
                    animais_pavao.setLooping(true);
                }

                if(btnAnimais10.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "animais_pavao");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnAnimais10.setChecked(true);
                    animais_pavao.start();
                    bPlaying = true;
                    btnAnimais10.setAlpha(1);
                }else {
                    animais_pavao.pause();
                    bPlaying = false;
                    btnAnimais10.setAlpha(0.3f);
                }
            }
        });

        btnInstrumentos1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(instrumentos_espanta_espiritos == null) {
                    instrumentos_espanta_espiritos = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.instrumentos_espanta_espiritos);
                    instrumentos_espanta_espiritos.setLooping(true);
                }

                if(btnInstrumentos1.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "instrumentos_espanta_espiritos");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnInstrumentos1.setChecked(true);
                    instrumentos_espanta_espiritos.start();
                    bPlaying = true;
                    btnInstrumentos1.setAlpha(1);
                }else {
                    instrumentos_espanta_espiritos.pause();
                    bPlaying = false;
                    btnInstrumentos1.setAlpha(0.3f);
                }
            }
        });

        btnInstrumentos4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(instrumentos_violao == null) {
                    instrumentos_violao = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.instrumentos_violao);
                    instrumentos_violao.setLooping(true);
                }

                if(btnInstrumentos4.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "instrumentos_violao");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnInstrumentos4.setChecked(true);
                    instrumentos_violao.start();
                    bPlaying = true;
                    btnInstrumentos4.setAlpha(1);
                }else {
                    instrumentos_violao.pause();
                    bPlaying = false;
                    btnInstrumentos4.setAlpha(0.3f);
                }
            }
        });

        btnTempo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(tempo_vento == null) {
                    tempo_vento = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.tempo_vento);
                    tempo_vento.setLooping(true);
                }

                if(btnTempo3.isChecked()) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("som", "tempo_vento");
                    //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                    btnTempo3.setChecked(true);
                    tempo_vento.start();
                    bPlaying = true;
                    btnTempo3.setAlpha(1);
                }else {
                    tempo_vento.pause();
                    bPlaying = false;
                    btnTempo3.setAlpha(0.3f);
                }
            }
        });


        if(globalMethods.FREE) {
            //Propagandas
            /*
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            */
            //


            llAmbientes2FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes2_FREE);
            llAmbientes3FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes3_FREE);
            llAmbientes4FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes4_FREE);
            llAmbientes6FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes6_FREE);
            llAnimais3FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais3_FREE);
            llAnimais4FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais4_FREE);
            llAnimais6FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais6_FREE);
            llAnimais7FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais7_FREE);
            llAnimais8FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais8_FREE);
            llIinstrumentos2FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos2_FREE);
            llIinstrumentos3FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos3_FREE);
            llIinstrumentos5FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos5_FREE);
            llTempo1FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llTempo1_FREE);
            llTempo2FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llTempo2_FREE);

            llAmbientes2FREE.setVisibility(View.VISIBLE);
            llAmbientes3FREE.setVisibility(View.VISIBLE);
            llAmbientes4FREE.setVisibility(View.VISIBLE);
            llAmbientes6FREE.setVisibility(View.VISIBLE);
            llAnimais3FREE.setVisibility(View.VISIBLE);
            llAnimais4FREE.setVisibility(View.VISIBLE);
            llAnimais6FREE.setVisibility(View.VISIBLE);
            llAnimais7FREE.setVisibility(View.VISIBLE);
            llAnimais8FREE.setVisibility(View.VISIBLE);
            llIinstrumentos2FREE.setVisibility(View.VISIBLE);
            llIinstrumentos3FREE.setVisibility(View.VISIBLE);
            llIinstrumentos5FREE.setVisibility(View.VISIBLE);
            llTempo1FREE.setVisibility(View.VISIBLE);
            llTempo2FREE.setVisibility(View.VISIBLE);
            //
        } else{
            llAmbientes2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes2);
            llAmbientes3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes3);
            llAmbientes4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes4);
            llAmbientes6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAmbientes6);
            llAnimais3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais3);
            llAnimais4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais4);
            llAnimais6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais6);
            llAnimais7 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais7);
            llAnimais8 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llAnimais8);
            llIinstrumentos2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos2);
            llIinstrumentos3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos3);
            llIinstrumentos5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llIinstrumentos5);
            llTempo1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llTempo1);
            llTempo2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llTempo2);

            llAmbientes2.setVisibility(View.VISIBLE);
            llAmbientes3.setVisibility(View.VISIBLE);
            llAmbientes4.setVisibility(View.VISIBLE);
            llAmbientes6.setVisibility(View.VISIBLE);
            llAnimais3.setVisibility(View.VISIBLE);
            llAnimais4.setVisibility(View.VISIBLE);
            llAnimais6.setVisibility(View.VISIBLE);
            llAnimais7.setVisibility(View.VISIBLE);
            llAnimais8.setVisibility(View.VISIBLE);
            llIinstrumentos2.setVisibility(View.VISIBLE);
            llIinstrumentos3.setVisibility(View.VISIBLE);
            llIinstrumentos5.setVisibility(View.VISIBLE);
            llTempo1.setVisibility(View.VISIBLE);
            llTempo2.setVisibility(View.VISIBLE);

            sbVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    volume = ((float) progress / 10);

                    MediaPlayerManager.getInstance().setVolume(volume);

                    if(ambientes_floresta_dia != null)
                        ambientes_floresta_dia.setVolume(volume, volume);
                    if(ambientes_floresta_noite != null)
                        ambientes_floresta_noite.setVolume(volume, volume);
                    if(ambientes_cachoeira != null)
                        ambientes_cachoeira.setVolume(volume, volume);
                    if(ambientes_caverna != null)
                        ambientes_caverna.setVolume(volume, volume);
                    if(ambientes_fogueira != null)
                        ambientes_fogueira.setVolume(volume, volume);
                    if(ambientes_profundezas != null)
                        ambientes_profundezas.setVolume(volume, volume);
                    if(animais_baleia != null)
                        animais_baleia.setVolume(volume, volume);
                    if(animais_lobo != null)
                        animais_lobo.setVolume(volume, volume);
                    if(animais_grilo != null)
                        animais_grilo.setVolume(volume, volume);
                    if(animais_coruja != null)
                        animais_coruja.setVolume(volume, volume);
                    if(animais_ovelha != null)
                        animais_ovelha.setVolume(volume, volume);
                    if(animais_sapo != null)
                        animais_sapo.setVolume(volume, volume);
                    if(animais_gato != null)
                        animais_gato.setVolume(volume, volume);
                    if(animais_cigarra != null)
                        animais_cigarra.setVolume(volume, volume);
                    if(animais_cavalo != null)
                        animais_cavalo.setVolume(volume, volume);
                    if(animais_pavao != null)
                        animais_pavao.setVolume(volume, volume);
                    if(instrumentos_espanta_espiritos != null)
                        instrumentos_espanta_espiritos.setVolume(volume, volume);
                    if(instrumentos_caixinha != null)
                        instrumentos_caixinha.setVolume(volume, volume);
                    if(instrumentos_flauta != null)
                        instrumentos_flauta.setVolume(volume, volume);
                    if(instrumentos_violao != null)
                        instrumentos_violao.setVolume(volume, volume);
                    if(instrumentos_sino_chines != null)
                        instrumentos_sino_chines.setVolume(volume, volume);
                    if(tempo_chuva != null)
                        tempo_chuva.setVolume(volume, volume);
                    if(tempo_trovao != null)
                        tempo_trovao.setVolume(volume, volume);
                    if(tempo_vento != null)
                        tempo_vento.setVolume(volume, volume);
                }
            });


            btnAmbientes2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(ambientes_floresta_noite == null) {
                        ambientes_floresta_noite = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_floresta_noite); //FREE
                        ambientes_floresta_noite.setLooping(true);
                        ambientes_floresta_noite.setVolume(volume, volume);
                    }

                    if(btnAmbientes2.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "ambientes_floresta_noite");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAmbientes2.setChecked(true);
                        ambientes_floresta_noite.start();
                        bPlaying = true;
                        btnAmbientes2.setAlpha(1);
                    }else {
                        ambientes_floresta_noite.pause();
                        bPlaying = false;
                        btnAmbientes2.setAlpha(0.3f);
                    }
                }
            });

            btnAmbientes3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(ambientes_cachoeira == null) {
                        ambientes_cachoeira = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_cachoeira); //FREE
                        ambientes_cachoeira.setLooping(true);
                        ambientes_cachoeira.setVolume(volume, volume);
                    }

                    if(btnAmbientes3.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "ambientes_cachoeira");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAmbientes3.setChecked(true);
                        ambientes_cachoeira.start();
                        bPlaying = true;
                        btnAmbientes3.setAlpha(1);
                    }else {
                        ambientes_cachoeira.pause();
                        bPlaying = false;
                        btnAmbientes3.setAlpha(0.3f);
                    }
                }
            });

            btnAmbientes4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(ambientes_caverna == null) {
                        ambientes_caverna = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_caverna);
                        ambientes_caverna.setLooping(true);
                        ambientes_caverna.setVolume(volume, volume);
                    }

                    if(btnAmbientes4.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "ambientes_caverna");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAmbientes4.setChecked(true);
                        ambientes_caverna.start();
                        bPlaying = true;
                        btnAmbientes4.setAlpha(1);
                    }else {
                        ambientes_caverna.pause();
                        bPlaying = false;
                        btnAmbientes4.setAlpha(0.3f);
                    }
                }
            });

            btnAmbientes6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(ambientes_praia == null) {
                        ambientes_praia = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_praia); //FREE
                        ambientes_praia.setLooping(true);
                        ambientes_praia.setVolume(volume, volume);
                    }

                    if(btnAmbientes6.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "ambientes_praia");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAmbientes6.setChecked(true);
                        ambientes_praia.start();
                        bPlaying = true;
                        btnAmbientes6.setAlpha(1);
                    }else {
                        ambientes_praia.pause();
                        bPlaying = false;
                        btnAmbientes6.setAlpha(0.3f);
                    }
                }
            });

            btnAmbientes7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(ambientes_rio == null) {
                        ambientes_rio = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.ambientes_rio); //FREE
                        ambientes_rio.setLooping(true);
                        ambientes_rio.setVolume(volume, volume);
                    }

                    if(btnAmbientes7.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "ambientes_rio");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAmbientes7.setChecked(true);
                        ambientes_rio.start();
                        bPlaying = true;
                        btnAmbientes7.setAlpha(1);
                    }else {
                        ambientes_rio.pause();
                        bPlaying = false;
                        btnAmbientes7.setAlpha(0.3f);
                    }
                }
            });

            btnAnimais3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(animais_lobo == null) {
                        animais_lobo = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_lobo); //FREE
                        animais_lobo.setLooping(true);
                        animais_lobo.setVolume(volume, volume);
                    }

                    if(btnAnimais3.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "animais_lobo");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAnimais3.setChecked(true);
                        animais_lobo.start();
                        bPlaying = true;
                        btnAnimais3.setAlpha(1);
                    }else {
                        animais_lobo.pause();
                        bPlaying = false;
                        btnAnimais3.setAlpha(0.3f);
                    }
                }
            });

            btnAnimais4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(animais_grilo == null) {
                        animais_grilo = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_grilo); //FREE
                        animais_grilo.setLooping(true);
                        animais_grilo.setVolume(volume, volume);
                    }

                    if(btnAnimais4.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "animais_grilo");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAnimais4.setChecked(true);
                        animais_grilo.start();
                        bPlaying = true;
                        btnAnimais4.setAlpha(1);
                    }else {
                        animais_grilo.pause();
                        bPlaying = false;
                        btnAnimais4.setAlpha(0.3f);
                    }
                }
            });

            btnAnimais6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(animais_ovelha == null) {
                        animais_ovelha = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_ovelha); //FREE
                        animais_ovelha.setLooping(true);
                        animais_ovelha.setVolume(volume, volume);
                    }

                    if(btnAnimais6.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "animais_ovelha");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAnimais6.setChecked(true);
                        animais_ovelha.start();
                        bPlaying = true;
                        btnAnimais6.setAlpha(1);
                    }else {
                        animais_ovelha.pause();
                        bPlaying = false;
                        btnAnimais6.setAlpha(0.3f);
                    }
                }
            });

            btnAnimais7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(animais_sapo == null) {
                        animais_sapo = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_sapo); //FREE
                        animais_sapo.setLooping(true);
                        animais_sapo.setVolume(volume, volume);
                    }

                    if(btnAnimais7.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "animais_sapo");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAnimais7.setChecked(true);
                        animais_sapo.start();
                        bPlaying = true;
                        btnAnimais7.setAlpha(1);
                    }else {
                        animais_sapo.pause();
                        bPlaying = false;
                        btnAnimais7.setAlpha(0.3f);
                    }
                }
            });

            btnAnimais8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(animais_gato == null) {
                        animais_gato = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.animais_gato); //FREE
                        animais_gato.setLooping(true);
                        animais_gato.setVolume(volume, volume);
                    }

                    if(btnAnimais8.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "animais_gato");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnAnimais8.setChecked(true);
                        animais_gato.start();
                        bPlaying = true;
                        btnAnimais8.setAlpha(1);
                    }else {
                        animais_gato.pause();
                        bPlaying = false;
                        btnAnimais8.setAlpha(0.3f);
                    }
                }
            });

            btnInstrumentos2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(instrumentos_caixinha == null){
                        instrumentos_caixinha = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.instrumentos_caixinha); //FREE
                        instrumentos_caixinha.setLooping(true);
                        instrumentos_caixinha.setVolume(volume, volume);
                    }

                    if(btnInstrumentos2.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "instrumentos_caixinha");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnInstrumentos2.setChecked(true);
                        instrumentos_caixinha.start();
                        bPlaying = true;
                        btnInstrumentos2.setAlpha(1);
                    }else {
                        instrumentos_caixinha.pause();
                        bPlaying = false;
                        btnInstrumentos2.setAlpha(0.3f);
                    }
                }
            });

            btnInstrumentos3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(instrumentos_flauta == null) {
                        instrumentos_flauta = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.instrumentos_flauta); //FREE
                        instrumentos_flauta.setLooping(true);
                        instrumentos_flauta.setVolume(volume, volume);
                    }

                    if(btnInstrumentos3.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "instrumentos_flauta");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnInstrumentos3.setChecked(true);
                        instrumentos_flauta.start();
                        bPlaying = true;
                        btnInstrumentos3.setAlpha(1);
                    }else {
                        instrumentos_flauta.pause();
                        bPlaying = false;
                        btnInstrumentos3.setAlpha(0.3f);
                    }
                }
            });

            btnInstrumentos5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(instrumentos_sino_chines == null) {
                        instrumentos_sino_chines = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.instrumentos_sino_chines); //FREE
                        instrumentos_sino_chines.setLooping(true);
                        instrumentos_sino_chines.setVolume(volume, volume);
                    }

                    if(btnInstrumentos5.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "instrumentos_sino_chines");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnInstrumentos5.setChecked(true);
                        instrumentos_sino_chines.start();
                        bPlaying = true;
                        btnInstrumentos5.setAlpha(1);
                    }else {
                        instrumentos_sino_chines.pause();
                        bPlaying = false;
                        btnInstrumentos5.setAlpha(0.3f);
                    }
                }
            });

            btnTempo1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(tempo_chuva == null) {
                        tempo_chuva = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.tempo_chuva);
                        tempo_chuva.setLooping(true);
                        tempo_chuva.setVolume(volume, volume);
                    }


                    if(btnTempo1.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "tempo_chuva");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnTempo1.setChecked(true);
                        tempo_chuva.start();
                        bPlaying = true;
                        btnTempo1.setAlpha(1);
                    }else {
                        tempo_chuva.pause();
                        bPlaying = false;
                        btnTempo1.setAlpha(0.3f);
                    }
                }
            });

            btnTempo2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(tempo_trovao == null) {
                        tempo_trovao = MediaPlayer.create(app_sons.this, br.com.relaxapp.R.raw.tempo_trovao);
                        tempo_trovao.setLooping(true);
                        tempo_trovao.setVolume(volume, volume);
                    }

                    if(btnTempo2.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putString("som", "tempo_trovao");
                        //firebaseAnalytics.logEvent("sons_start", bundleFirebaseAnalitics);

                        btnTempo2.setChecked(true);
                        tempo_trovao.start();
                        bPlaying = true;
                        btnTempo2.setAlpha(1);
                    }else {
                        tempo_trovao.pause();
                        bPlaying = false;
                        btnTempo2.setAlpha(0.3f);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        voltar();
    }

    public void voltar() {
        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();

        if(bPlaying) {
            intent = new Intent(app_sons.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else
            finish();
    }

    @Override
    protected void onStop() {
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;

            if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
            }
        }
        super.onStop();
    }

    public void llClick(View view){
        if (view.getId() == br.com.relaxapp.R.id.llAmbientes1)
            btnAmbientes1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes2)
            btnAmbientes2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes3)
            btnAmbientes3.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes4)
            btnAmbientes4.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes5)
            btnAmbientes5.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes6)
            btnAmbientes6.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes7)
            btnAmbientes7.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmbientes8)
            btnAmbientes8.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais1)
            btnAnimais1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais2)
            btnAnimais2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais3)
            btnAnimais3.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais4)
            btnAnimais4.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais5)
            btnAnimais5.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais6)
            btnAnimais6.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais7)
            btnAnimais7.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais8)
            btnAnimais8.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais9)
            btnAnimais9.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAnimais10)
            btnAnimais10.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIinstrumentos1)
            btnInstrumentos1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIinstrumentos2)
            btnInstrumentos2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIinstrumentos3)
            btnInstrumentos3.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIinstrumentos4)
            btnInstrumentos4.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIinstrumentos5)
            btnInstrumentos5.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llTempo1)
            btnTempo1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llTempo2)
            btnTempo2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llTempo3)
            btnTempo3.performClick();
        else
            globalMethods.mostrarPRO();
    }
}