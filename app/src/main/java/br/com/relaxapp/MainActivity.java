package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.content.Intent;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    GlobalMethods globalMethods;
    Intent intent;
    DrawerLayout drawerLayout;

    TabSpec spec1;
    TabSpec spec2;
    TabSpec spec3;
    TabSpec spec4;

    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;
    AlertDialog.Builder dlg;

    Spinner spiAno;

    String sqlConsultaConfiguracoes = "SELECT * FROM CONFIGURACOES";

    Boolean acessouConfiguracoes = false;

    EditText edtNome;
    LinearLayout llPanico;
    Switch swPanico;
    boolean bAutorizandoOverlay = false;
    TextView tvPanicoStatus;
    TextView txtNrNotificacoes;
    SeekBar sbNotificacoes;
    CheckBox cbDom;
    CheckBox cbSeg;
    CheckBox cbTer;
    CheckBox cbQua;
    CheckBox cbQui;
    CheckBox cbSex;
    CheckBox cbSab;
    EditText edtNotificacoesInicioHora;
    EditText edtNotificacoesInicioMinuto;
    EditText edtNotificacoesFimHora;
    EditText edtNotificacoesFimMinuto;
    Integer i, horaIni, minIni, horaFim, minFim;

    private static final int APP_PERMISSION_REQUEST = 102;

    protected BarChart bcAnsiedade;
    protected BarChart bcMelhora;

    //private LinearLayout layAdView;
    //private AdView mAdView;

    //private FirebaseAnalytics firebaseAnalytics;
    //private Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(br.com.relaxapp.R.layout.activity_main);

        globalMethods = new GlobalMethods(this);

        globalMethods.contabilizarLog();

        Toolbar toolbar = (Toolbar) findViewById(br.com.relaxapp.R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(br.com.relaxapp.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, br.com.relaxapp.R.string.navigation_drawer_open, br.com.relaxapp.R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(br.com.relaxapp.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Relógio Transparente
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );

        /*
        if(globalMethods.FREE) {
            //Propagandas
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            //
        }
        */

        //Controlar a cor do relógio quando abrir menu
        drawer.setDrawerListener(new DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {
            }

            @Override
            public void onDrawerOpened(View view) {
                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSplash));
                    getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSplash));
                }
            }

            @Override
            public void onDrawerClosed(View view) {
                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorPrimaryDark));
                    getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorPrimaryDark));
                }
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });


        //Abas
        final TabHost tabHost = (TabHost) findViewById(br.com.relaxapp.R.id.tabHost);
        tabHost.setup();

        spec1 = tabHost.newTabSpec("Aba 1");
        spec1.setContent(br.com.relaxapp.R.id.tab1);
        spec1.setIndicator("", getResources().getDrawable(br.com.relaxapp.R.drawable.ic_home));


        spec2 = tabHost.newTabSpec("Aba 2");
        spec2.setIndicator("", getResources().getDrawable(br.com.relaxapp.R.drawable.ic_graphic));
        spec2.setContent(br.com.relaxapp.R.id.tab2);

        spec3 = tabHost.newTabSpec("Aba 4");
        spec3.setIndicator("", getResources().getDrawable(br.com.relaxapp.R.drawable.ic_share));
        spec3.setContent(br.com.relaxapp.R.id.tab3);

        spec4 = tabHost.newTabSpec("Aba 5");
        spec4.setIndicator("", getResources().getDrawable(br.com.relaxapp.R.drawable.ic_config));
        spec4.setContent(br.com.relaxapp.R.id.tab4);


        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.addTab(spec4);

        setupNavigationDrawerContent(navigationView);

        spiAno = (Spinner) findViewById(br.com.relaxapp.R.id.spiAno);

        edtNome = (EditText) findViewById(br.com.relaxapp.R.id.edtNome);
        sbNotificacoes = (SeekBar) findViewById(br.com.relaxapp.R.id.sbNotificacoes);
        llPanico = (LinearLayout) findViewById(br.com.relaxapp.R.id.llPanico);
        swPanico = (Switch) findViewById(br.com.relaxapp.R.id.swPanico);
        tvPanicoStatus = (TextView) findViewById(br.com.relaxapp.R.id.tvPanicoStatus);
        txtNrNotificacoes = (TextView) findViewById(br.com.relaxapp.R.id.txtNrNotificacoes);
        cbDom = (CheckBox) findViewById(br.com.relaxapp.R.id.cbDom);
        cbSeg = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSeg);
        cbTer = (CheckBox) findViewById(br.com.relaxapp.R.id.cbTer);
        cbQua = (CheckBox) findViewById(br.com.relaxapp.R.id.cbQua);
        cbQui = (CheckBox) findViewById(br.com.relaxapp.R.id.cbQui);
        cbSex = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSex);
        cbSab = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSab);
        cbDom = (CheckBox) findViewById(br.com.relaxapp.R.id.cbDom);
        edtNotificacoesInicioHora = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesInicioHora);
        edtNotificacoesInicioMinuto = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesInicioMinuto);
        edtNotificacoesFimHora = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesFimHora);
        edtNotificacoesFimMinuto = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesFimMinuto);


        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                switch (tabHost.getCurrentTab()) {
                    case 0:
                        if (acessouConfiguracoes)
                            salvarConfiguracoes();
                        break;
                    case 1:
                        carregarGraficos();

                        if (acessouConfiguracoes)
                            salvarConfiguracoes();
                        break;
                    case 2:
                        if (acessouConfiguracoes)
                            salvarConfiguracoes();
                        break;
                    case 3:
                        acessouConfiguracoes = true;
                        carregarConfiguracoes();
                        break;

                    default:

                        break;
                }
            }
        });


        llPanico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swPanico.performClick();
            }
        });

        swPanico.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvPanicoStatus.setText(getString(br.com.relaxapp.R.string.ativado));
                    chamarBotaoPanico();
                } else {
                    tvPanicoStatus.setText(getString(br.com.relaxapp.R.string.desativado));
                    stopService(new Intent(MainActivity.this, FloatWidgetService.class));
                }
            }
        });

        sbNotificacoes.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtNrNotificacoes.setText(String.valueOf(sbNotificacoes.getProgress()));
            }
        });

        spiAno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                carregarGraficos();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }

        });

        Locale locale = Locale.getDefault();
        if(!(locale.getLanguage().equals("pt")||locale.getLanguage().equals("en"))){
            TableLayout tlHide;
            tlHide = (TableLayout) findViewById(br.com.relaxapp.R.id.layMentalizacao);
            tlHide.setVisibility(View.GONE);
            tlHide = (TableLayout) findViewById(R.id.layRespiracao);
            tlHide.setVisibility(View.GONE);
            tlHide = (TableLayout) findViewById(R.id.layMeditacao);
            tlHide.setVisibility(View.GONE);
        }

        Notification.generateNotification(this);
        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();
    }

    public void compartilhar(View view) throws IOException {
        try {
            final int REQUEST_EXTERNAL_STORAGE = 1;
            final String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            int writePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );

                SystemClock.sleep(2500);
            }

            Bitmap bm;

            Locale locale = Locale.getDefault();
            if(locale.getLanguage().equals("pt"))
                bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.campanha_pt);
            else if (locale.getLanguage().equals("es"))
                bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.campanha_es);
            else
                bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.campanha_en);

            File imageFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/relaxapp.jpg");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(imageFile);

                bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                fos.close();


            } catch (IOException e) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("image/jpeg");

            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.compartilhar_extraText));
            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            share.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/relaxapp.jpg")));
            //share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Logo.png")));

            startActivity(Intent.createChooser(share, getString(R.string.compartilhar_selecione)));

            //bundleFirebaseAnalitics.clear();
            //firebaseAnalytics.logEvent("compartilhar_start", bundleFirebaseAnalitics);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getString(R.string.erro_compartilhar), Toast.LENGTH_SHORT).show();
        }
    }

    public void carregarConfiguracoes() {
        try {
            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlConsultaConfiguracoes, null);


            if (cursor.getCount() > 0) {

                cursor.moveToFirst();

                edtNome.setText(cursor.getString(cursor.getColumnIndex("nome")).trim());

                swPanico.setChecked(cursor.getString(cursor.getColumnIndex("panico")).trim().equals("S"));

                sbNotificacoes.setProgress(cursor.getInt(cursor.getColumnIndex("notificacoes")));

                cbDom.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_dom")).trim().equals("S"));
                cbSeg.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_seg")).trim().equals("S"));
                cbTer.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_ter")).trim().equals("S"));
                cbQua.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_qua")).trim().equals("S"));
                cbQui.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_qui")).trim().equals("S"));
                cbSex.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_sex")).trim().equals("S"));
                cbSab.setChecked(cursor.getString(cursor.getColumnIndex("notificacao_sab")).trim().equals("S"));

                edtNotificacoesInicioHora.setText(String.format("%02d", cursor.getInt(cursor.getColumnIndex("notificacao_hora_inicio"))));
                edtNotificacoesInicioMinuto.setText(String.format("%02d", cursor.getInt(cursor.getColumnIndex("notificacao_min_inicio"))));
                edtNotificacoesFimHora.setText(String.format("%02d", cursor.getInt(cursor.getColumnIndex("notificacao_hora_fim"))));
                edtNotificacoesFimMinuto.setText(String.format("%02d", cursor.getInt(cursor.getColumnIndex("notificacao_min_fim"))));
            }
        } catch (SQLException ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage(br.com.relaxapp.R.string.erro_BD);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        }
    }

    public void salvarConfiguracoes() {

        horaIni = Integer.parseInt(edtNotificacoesInicioHora.getText().toString());
        minIni = Integer.parseInt(edtNotificacoesInicioMinuto.getText().toString());
        horaFim = Integer.parseInt(edtNotificacoesFimHora.getText().toString());
        minFim = Integer.parseInt(edtNotificacoesFimMinuto.getText().toString());

        if ((edtNotificacoesInicioHora.getText().toString().trim().length() == 0) || (horaIni < 0) || (horaIni > 24)) {
            Toast.makeText(MainActivity.this, getString(br.com.relaxapp.R.string.erro_hora), Toast.LENGTH_SHORT).show();
            carregarConfiguracoes();
        } else if ((edtNotificacoesInicioMinuto.getText().toString().trim().length() == 0) || (minIni < 0) || (minIni > 59)) {
            Toast.makeText(MainActivity.this, getString(br.com.relaxapp.R.string.erro_minuto), Toast.LENGTH_SHORT).show();
            carregarConfiguracoes();
        } else if ((edtNotificacoesFimHora.getText().toString().trim().length() == 0) || (horaFim < 0) || (horaFim > 24)) {
            Toast.makeText(MainActivity.this, getString(br.com.relaxapp.R.string.erro_hora), Toast.LENGTH_SHORT).show();
            carregarConfiguracoes();
        } else if ((edtNotificacoesFimMinuto.getText().toString().trim().length() == 0) || (minFim < 0) || (minFim > 59)) {
            Toast.makeText(MainActivity.this, getString(br.com.relaxapp.R.string.erro_minuto), Toast.LENGTH_SHORT).show();
            carregarConfiguracoes();
        } else {
            try {
                dataBase = new DataBase(MainActivity.this);
                conn = dataBase.getWritableDatabase();

                ContentValues row = new ContentValues();

                row.put("nome", edtNome.getText().toString());

                row.put("notificacoes", sbNotificacoes.getProgress());

                if (cbDom.isChecked())
                    row.put("notificacao_dom", "S");
                else
                    row.put("notificacao_dom", "N");

                if (cbSeg.isChecked())
                    row.put("notificacao_seg", "S");
                else
                    row.put("notificacao_seg", "N");

                if (cbTer.isChecked())
                    row.put("notificacao_ter", "S");
                else
                    row.put("notificacao_ter", "N");

                if (cbQua.isChecked())
                    row.put("notificacao_qua", "S");
                else
                    row.put("notificacao_qua", "N");

                if (cbQui.isChecked())
                    row.put("notificacao_qui", "S");
                else
                    row.put("notificacao_qui", "N");

                if (cbSex.isChecked())
                    row.put("notificacao_sex", "S");
                else
                    row.put("notificacao_sex", "N");

                if (cbSab.isChecked())
                    row.put("notificacao_sab", "S");
                else
                    row.put("notificacao_sab", "N");

                row.put("notificacao_hora_inicio", horaIni);

                row.put("notificacao_min_inicio", minIni);

                row.put("notificacao_hora_fim", horaFim);

                row.put("notificacao_min_fim", minFim);

                if (swPanico.isChecked())
                    row.put("panico", "S");
                else
                    row.put("panico", "N");

                conn.update("CONFIGURACOES", row, null, null);

                globalMethods.setarNotificacoes(
                        sbNotificacoes.getProgress(),
                        horaIni,
                        minIni,
                        horaFim,
                        minFim,
                        cbDom.isChecked(),
                        cbSeg.isChecked(),
                        cbTer.isChecked(),
                        cbQua.isChecked(),
                        cbQui.isChecked(),
                        cbSex.isChecked(),
                        cbSab.isChecked());

                Toast.makeText(getApplicationContext(), br.com.relaxapp.R.string.mainConfiguracoes_salvas, Toast.LENGTH_SHORT).show();

                acessouConfiguracoes = false;
            } catch (SQLException ex) {
                dlg = new AlertDialog.Builder(MainActivity.this);
                dlg.setMessage(getString(br.com.relaxapp.R.string.erro_BD));
                dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                dlg.show();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }


    protected void chamarBotaoPanico() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(MainActivity.this))
            startService(new Intent(MainActivity.this, FloatWidgetService.class));
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(MainActivity.this)) {
            bAutorizandoOverlay = true;
            swPanico.setChecked(false);
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, APP_PERMISSION_REQUEST);
            Toast.makeText(getApplicationContext(), getString(br.com.relaxapp.R.string.mainConfiguracoes_botaoPanicoAutorizacao), Toast.LENGTH_LONG).show();
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
            startService(new Intent(MainActivity.this, FloatWidgetService.class));
        else
            Toast.makeText(this, getString(br.com.relaxapp.R.string.erro_panico), Toast.LENGTH_SHORT).show();
    }

    public void carregarGraficos() {
        String sqlConsultaCheckIn = "SELECT * FROM CHECKIN";

        Date date;
        String formatedMonth, formatedYear;

        int iMes, iAno, iRegistros;
        int iQtdJan = 0, iQtdFev = 0, iQtdMar = 0, iQtdAbr = 0, iQtdMai = 0, iQtdJun = 0, iQtdJul = 0,
                iQtdAgo = 0, iQtdSet = 0, iQtdOut = 0, iQtdNov = 0, iQtdDez = 0;
        int iAnsiedadeJan = 0, iAnsiedadeFev = 0, iAnsiedadeMar = 0, iAnsiedadeAbr = 0, iAnsiedadeMai = 0, iAnsiedadeJun = 0, iAnsiedadeJul = 0,
                iAnsiedadeAgo = 0, iAnsiedadeSet = 0, iAnsiedadeOut = 0, iAnsiedadeNov = 0, iAnsiedadeDez = 0;
        int iMelhoraJan = 0, iMelhoraFev = 0, iMelhoraMar = 0, iMelhoraAbr = 0, iMelhoraMai = 0, iMelhoraJun = 0, iMelhoraJul = 0,
                iMelhoraAgo = 0, iMelhoraSet = 0, iMelhoraOut = 0, iMelhoraNov = 0, iMelhoraDez = 0;

        try {
            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlConsultaCheckIn, null);

            iRegistros = cursor.getCount();

            cursor.moveToFirst();

            for (i = 0; i < iRegistros; i++) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                date = sdf.parse(cursor.getString(cursor.getColumnIndex("data")).trim());

                formatedYear = new SimpleDateFormat("yyyy").format(date);
                iAno = Integer.parseInt(formatedYear);

                if(iAno == Integer.parseInt(spiAno.getSelectedItem().toString())) {
                    formatedMonth = new SimpleDateFormat("MM").format(date);
                    iMes = Integer.parseInt(formatedMonth);

                    switch (iMes) {
                        case 1:
                            iQtdJan++;
                            iAnsiedadeJan = iAnsiedadeJan + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraJan = iMelhoraJan + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 2:
                            iQtdFev++;
                            iAnsiedadeFev = iAnsiedadeFev + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraFev = iMelhoraFev + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 3:
                            iQtdMar++;
                            iAnsiedadeMar = iAnsiedadeMar + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraMar = iMelhoraMar + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 4:
                            iQtdAbr++;
                            iAnsiedadeAbr = iAnsiedadeAbr + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraAbr = iMelhoraAbr + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 5:
                            iQtdMai++;
                            iAnsiedadeMai = iAnsiedadeMai + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraMai = iMelhoraMai + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 6:
                            iQtdJun++;
                            iAnsiedadeJun = iAnsiedadeJun + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraJun = iMelhoraJun + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 7:
                            iQtdJul++;
                            iAnsiedadeJul = iAnsiedadeJul + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraJul = iMelhoraJul + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 8:
                            iQtdAgo++;
                            iAnsiedadeAgo = iAnsiedadeAgo + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraAgo = iMelhoraAgo + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 9:
                            iQtdSet++;
                            iAnsiedadeSet = iAnsiedadeSet + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraSet = iMelhoraSet + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 10:
                            iQtdOut++;
                            iAnsiedadeOut = iAnsiedadeOut + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraOut = iMelhoraOut + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 11:
                            iQtdNov++;
                            iAnsiedadeNov = iAnsiedadeNov + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraNov = iMelhoraNov + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        case 12:
                            iQtdDez++;
                            iAnsiedadeDez = iAnsiedadeDez + cursor.getInt(cursor.getColumnIndex("ansiedade"));
                            iMelhoraDez = iMelhoraDez + cursor.getInt(cursor.getColumnIndex("resultado"));
                            break;
                        default:
                            break;
                    }
                }

                cursor.moveToNext();
            }

            if (iAnsiedadeJan > 0)
                iAnsiedadeJan = (iAnsiedadeJan / iRegistros) * iQtdJan;
            if (iMelhoraJan > 0)
                iMelhoraJan = (int) ((((iAnsiedadeJan + (iMelhoraJan * 2.2)) / 2) / iRegistros) * iQtdJan);


            if (iAnsiedadeFev > 0)
                iAnsiedadeFev = (iAnsiedadeFev / iRegistros) * iQtdFev;
            if (iMelhoraFev > 0)
                iMelhoraFev = (int) ((((iAnsiedadeFev + (iMelhoraFev * 2.2)) / 2) / iRegistros) * iQtdFev);


            if (iAnsiedadeMar > 0)
                iAnsiedadeMar = (iAnsiedadeMar / iRegistros) * iQtdMar;
            if (iMelhoraMar > 0)
                iMelhoraMar = (int) ((((iAnsiedadeMar + (iMelhoraMar * 2.2)) / 2) / iRegistros) * iQtdMar);


            if (iAnsiedadeAbr > 0)
                iAnsiedadeAbr = (iAnsiedadeAbr / iRegistros) * iQtdAbr;
            if (iMelhoraAbr > 0)
                iMelhoraAbr = (int) ((((iAnsiedadeAbr + (iMelhoraAbr * 2.2)) / 2) / iRegistros) * iQtdAbr);


            if (iAnsiedadeMai > 0)
                iAnsiedadeMai = (iAnsiedadeMai / iRegistros) * iQtdMai;
            if (iMelhoraMai > 0)
                iMelhoraMai = (int) ((((iAnsiedadeMai + (iMelhoraMai * 2.2)) / 2) / iRegistros) * iQtdMai);


            if (iAnsiedadeJun > 0)
                iAnsiedadeJun = (iAnsiedadeJun / iRegistros) * iQtdJun;
            if (iMelhoraJun > 0)
                iMelhoraJun = (int) ((((iAnsiedadeJun + (iMelhoraJun * 2.2)) / 2) / iRegistros) * iQtdJun);


            if (iAnsiedadeJul > 0)
                iAnsiedadeJul = (iAnsiedadeJul / iRegistros) * iQtdJul;
            if (iMelhoraJul > 0)
                iMelhoraJul = (int) ((((iAnsiedadeJul + (iMelhoraJul * 2.2)) / 2) / iRegistros) * iQtdJul);


            if (iAnsiedadeAgo > 0)
                iAnsiedadeAgo = (iAnsiedadeAgo / iRegistros) * iQtdAgo;
            if (iMelhoraAgo > 0)
                iMelhoraAgo = (int) ((((iAnsiedadeAgo + (iMelhoraAgo * 2.2)) / 2) / iRegistros) * iQtdAgo);


            if (iAnsiedadeSet > 0)
                iAnsiedadeSet = (iAnsiedadeSet / iRegistros) * iQtdSet;
            if (iMelhoraSet > 0)
                iMelhoraSet = (int) ((((iAnsiedadeSet + (iMelhoraSet * 2.2)) / 2) / iRegistros) * iQtdSet);


            if (iAnsiedadeOut > 0)
                iAnsiedadeOut = (iAnsiedadeOut / iRegistros) * iQtdOut;
            if (iMelhoraOut > 0)
                iMelhoraOut = (int) ((((iAnsiedadeOut + (iMelhoraOut * 2.2)) / 2) / iRegistros) * iQtdOut);


            if (iAnsiedadeNov > 0)
                iAnsiedadeNov = (iAnsiedadeNov / iRegistros) * iQtdNov;
            if (iMelhoraNov > 0)
                iMelhoraNov = (int) ((((iAnsiedadeNov + (iMelhoraNov * 2.2)) / 2) / iRegistros) * iQtdNov);


            if (iAnsiedadeDez > 0)
                iAnsiedadeDez = (iAnsiedadeDez / iRegistros) * iQtdDez;
            if (iMelhoraDez > 0)
                iMelhoraDez = (int) ((((iAnsiedadeDez + (iMelhoraDez * 2.2)) / 2) / iRegistros) * iQtdDez);

        } catch (SQLException ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage(br.com.relaxapp.R.string.erro_BD);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        } catch (ParseException e) {
            e.printStackTrace();
            dlg.setMessage(br.com.relaxapp.R.string.erro_conversao);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        }


        bcAnsiedade = (BarChart) findViewById(br.com.relaxapp.R.id.bcAnsiedade);
        bcMelhora = (BarChart) findViewById(br.com.relaxapp.R.id.bcMelhora);

        bcAnsiedade.animateY(3000);
        bcMelhora.animateY(3000);

        bcAnsiedade.setDescription("");
        bcMelhora.setDescription("");

        bcAnsiedade.setDrawGridBackground(false);
        bcMelhora.setDrawGridBackground(false);


        bcAnsiedade.setDrawBorders(false);


        ArrayList<BarEntry> beAnsiedade = new ArrayList<>();
        beAnsiedade.add(new BarEntry(iAnsiedadeJan, 0));
        beAnsiedade.add(new BarEntry(iAnsiedadeFev, 1));
        beAnsiedade.add(new BarEntry(iAnsiedadeMar, 2));
        beAnsiedade.add(new BarEntry(iAnsiedadeAbr, 3));
        beAnsiedade.add(new BarEntry(iAnsiedadeMai, 4));
        beAnsiedade.add(new BarEntry(iAnsiedadeJun, 5));
        beAnsiedade.add(new BarEntry(iAnsiedadeJul, 6));
        beAnsiedade.add(new BarEntry(iAnsiedadeAgo, 7));
        beAnsiedade.add(new BarEntry(iAnsiedadeSet, 8));
        beAnsiedade.add(new BarEntry(iAnsiedadeOut, 9));
        beAnsiedade.add(new BarEntry(iAnsiedadeNov, 10));
        beAnsiedade.add(new BarEntry(iAnsiedadeDez, 11));

        ArrayList<BarEntry> beMelhora = new ArrayList<>();
        beMelhora.add(new BarEntry(iMelhoraJan, 0));
        beMelhora.add(new BarEntry(iMelhoraFev, 1));
        beMelhora.add(new BarEntry(iMelhoraMar, 2));
        beMelhora.add(new BarEntry(iMelhoraAbr, 3));
        beMelhora.add(new BarEntry(iMelhoraMai, 4));
        beMelhora.add(new BarEntry(iMelhoraJun, 5));
        beMelhora.add(new BarEntry(iMelhoraJul, 6));
        beMelhora.add(new BarEntry(iMelhoraAgo, 7));
        beMelhora.add(new BarEntry(iMelhoraSet, 8));
        beMelhora.add(new BarEntry(iMelhoraOut, 9));
        beMelhora.add(new BarEntry(iMelhoraNov, 10));
        beMelhora.add(new BarEntry(iMelhoraDez, 11));

        BarDataSet bdsAnsiedade = new BarDataSet(beAnsiedade, "");
        BarDataSet bdsMelhora = new BarDataSet(beMelhora, "");

        bdsAnsiedade.setColors(ColorTemplate.COLORFUL_COLORS);
        bdsMelhora.setColors(ColorTemplate.COLORFUL_COLORS);

        ArrayList<String> labels = new ArrayList<String>();
        labels.add(getString(br.com.relaxapp.R.string.jan));
        labels.add(getString(br.com.relaxapp.R.string.fev));
        labels.add(getString(br.com.relaxapp.R.string.mar));
        labels.add(getString(br.com.relaxapp.R.string.abr));
        labels.add(getString(br.com.relaxapp.R.string.mai));
        labels.add(getString(br.com.relaxapp.R.string.jun));
        labels.add(getString(br.com.relaxapp.R.string.jul));
        labels.add(getString(br.com.relaxapp.R.string.ago));
        labels.add(getString(br.com.relaxapp.R.string.set));
        labels.add(getString(br.com.relaxapp.R.string.out));
        labels.add(getString(br.com.relaxapp.R.string.nov));
        labels.add(getString(br.com.relaxapp.R.string.dez));

        BarData bdAnsiedade = new BarData(labels, bdsAnsiedade);
        BarData bdMelhora = new BarData(labels, bdsMelhora);

        bcAnsiedade.setData(bdAnsiedade);
        bcMelhora.setData(bdMelhora);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(br.com.relaxapp.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeIntent);
        }
    }


    @Override
    protected void onStop() {
        if (!bAutorizandoOverlay) {
            Context context = getApplicationContext();
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

            if (!taskInfo.isEmpty()) {
                ComponentName topActivity = taskInfo.get(0).topActivity;

                if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                    if (acessouConfiguracoes)
                        salvarConfiguracoes();

                    finishAffinity();
                    android.os.Process.killProcess(android.os.Process.myPid());
                    //System.exit(0);
                }
            }

        }

        if (acessouConfiguracoes)
            salvarConfiguracoes();

        super.onStop();
    }

    @Override
    protected void onResume() {
        bAutorizandoOverlay = false;
        super.onResume();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        drawerLayout = (DrawerLayout) findViewById(br.com.relaxapp.R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case br.com.relaxapp.R.id.nav_checkIn:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_checkIn.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_mentalizacao:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_mentalizacao.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_respiracao:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_respiracao.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_meditacao:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_meditacao.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_frequenciasSonoras:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_frequencias.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_cromoterapia:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_cromoterapia.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_sonsRelaxantes:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_sons.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                                return true;
                            case br.com.relaxapp.R.id.nav_timer:
                                menuItem.setChecked(true);
                                intent = new Intent(MainActivity.this, app_timer.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                        }
                        return true;
                    }
                });

        Locale locale = Locale.getDefault();
        if(!(locale.getLanguage().equals("pt")||locale.getLanguage().equals("en"))) {
            navigationView.getMenu().findItem(br.com.relaxapp.R.id.nav_mentalizacao).setVisible(false);
            navigationView.getMenu().findItem(br.com.relaxapp.R.id.nav_respiracao).setVisible(false);
            navigationView.getMenu().findItem(br.com.relaxapp.R.id.nav_meditacao).setVisible(false);
        }

    }


    public void llClick(View view) {
        if (view.getId() == br.com.relaxapp.R.id.layCheckIn) {
            intent = new Intent(MainActivity.this, app_checkIn.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layMentalizacao) {
            intent = new Intent(MainActivity.this, app_mentalizacao.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layRespiracao){
            intent = new Intent(MainActivity.this, app_respiracao.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layMeditacao) {
            intent = new Intent(MainActivity.this, app_meditacao.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layFrequencias) {
            intent = new Intent(MainActivity.this, app_frequencias.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layCromoterapia) {
            intent = new Intent(MainActivity.this, app_cromoterapia.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.laySons) {
            intent = new Intent(MainActivity.this, app_sons.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layTimer) {
            intent = new Intent(MainActivity.this, app_timer.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else if (view.getId() == br.com.relaxapp.R.id.layMichelle) {
            try {
                String uri = "fb://page/319438928506780";
                Intent intentMi = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intentMi);
            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://facebook.com/DraMichelleBP")));
            }
        }else if (view.getId() == br.com.relaxapp.R.id.layTiago) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/in/tiagoschernoveber/")));
        }
    }
}


