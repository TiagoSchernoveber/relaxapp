package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.ToggleButton;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

public class app_frequencias extends Activity {
    GlobalMethods globalMethods;

    Intent intent;

    public static CountDownTimer contDownTimer;

    public static SeekBar sbVolume;
    float volume = 1;

    public static Boolean bPlaying = false;

    ToggleButton btnFrequencia1;
    ToggleButton btnFrequencia2;
    ToggleButton btnFrequencia3;
    ToggleButton btnFrequencia4;
    ToggleButton btnFrequencia5;
    ToggleButton btnFrequencia6;
    ToggleButton btnFrequencia7;
    ToggleButton btnFrequencia8;
    ToggleButton btnFrequencia9;
    ToggleButton btnFrequencia10;
    ToggleButton btnFrequencia11;
    ToggleButton btnFrequencia12;
    ToggleButton btnFrequencia13;
    ToggleButton btnFrequencia14;
    ToggleButton btnFrequencia15;
    ToggleButton btnFrequencia16;
    ToggleButton btnFrequencia17;
    ToggleButton btnFrequencia18;

    MediaPlayer delta005;
    MediaPlayer delta010;
    MediaPlayer delta015;
    MediaPlayer delta025;
    MediaPlayer delta030;
    MediaPlayer delta035;
    MediaPlayer delta040;
    MediaPlayer theta050;
    MediaPlayer theta070;
    MediaPlayer theta075;
    MediaPlayer alpha085;
    MediaPlayer alpha090;
    MediaPlayer alpha100;
    MediaPlayer alpha105;
    MediaPlayer beta140;
    MediaPlayer beta150;
    MediaPlayer beta180;
    MediaPlayer beta200;

    FrameLayout llearphones;

    LinearLayout llfrequencia2;
    LinearLayout llfrequencia2FREE;
    LinearLayout llfrequencia4;
    LinearLayout llfrequencia4FREE;
    LinearLayout llfrequencia6;
    LinearLayout llfrequencia6FREE;
    LinearLayout llfrequencia7;
    LinearLayout llfrequencia7FREE;
    LinearLayout llfrequencia8;
    LinearLayout llfrequencia8FREE;
    LinearLayout llfrequencia9;
    LinearLayout llfrequencia9FREE;
    LinearLayout llfrequencia11;
    LinearLayout llfrequencia11FREE;
    LinearLayout llfrequencia12;
    LinearLayout llfrequencia12FREE;
    LinearLayout llfrequencia15;
    LinearLayout llfrequencia15FREE;
    LinearLayout llfrequencia16;
    LinearLayout llfrequencia16FREE;
    LinearLayout llfrequencia17;
    LinearLayout llfrequencia17FREE;

    //private LinearLayout layAdView;
    //AdView mAdView;

    //private FirebaseAnalytics firebaseAnalytics;
    //private Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarFrequencias));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarFrequencias));
        }

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_frequencias);

        globalMethods = new GlobalMethods(this);

        llearphones = (FrameLayout) findViewById(br.com.relaxapp.R.id.llearphones);

        contDownTimer = new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                 }

            public void onFinish() {
                llearphones.setVisibility(View.GONE);
            }

        }.start();

        ImageView backFrequencias = (ImageView) findViewById(br.com.relaxapp.R.id.backFrequencias);
        backFrequencias.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                voltar();
            }
        });

        sbVolume = (SeekBar) findViewById(br.com.relaxapp.R.id.sbVolume);

        btnFrequencia1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia1);
        btnFrequencia2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia2);
        btnFrequencia3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia3);
        btnFrequencia4 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia4);
        btnFrequencia5 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia5);
        btnFrequencia6 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia6);
        btnFrequencia7 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia7);
        btnFrequencia8 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia8);
        btnFrequencia9 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia9);
        btnFrequencia10 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia10);
        btnFrequencia11 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia11);
        btnFrequencia12 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia12);
        btnFrequencia13 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia13);
        btnFrequencia14 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia14);
        btnFrequencia15 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia15);
        btnFrequencia16 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia16);
        btnFrequencia17 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia17);
        btnFrequencia18 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnFrequencia18);


        sbVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volume = ((float) progress / 10);

                if (globalMethods.FREE) {
                    if(delta005 != null)
                        delta005.setVolume(volume, volume);
                    if(delta015 != null)
                        delta015.setVolume(volume, volume);
                    if(delta030 != null)
                        delta030.setVolume(volume, volume);
                    if(theta075 != null)
                        theta075.setVolume(volume, volume);
                    if(alpha100 != null)
                        alpha100.setVolume(volume, volume);
                    if(alpha105 != null)
                        alpha105.setVolume(volume, volume);
                    if(beta200 != null)
                        beta200.setVolume(volume, volume);
                } else {
                    if(delta005 != null)
                        delta005.setVolume(volume, volume);
                    if(delta010 != null)
                        delta010.setVolume(volume, volume);
                    if(delta015 != null)
                        delta015.setVolume(volume, volume);
                    if(delta025 != null)
                        delta025.setVolume(volume, volume);
                    if(delta030 != null)
                        delta030.setVolume(volume, volume);
                    if(delta040 != null)
                        delta040.setVolume(volume, volume);
                    if(delta035 != null)
                        delta035.setVolume(volume, volume);
                    if(theta050 != null)
                        theta050.setVolume(volume, volume);
                    if(theta070 != null)
                        theta070.setVolume(volume, volume);
                    if(theta075 != null)
                        theta075.setVolume(volume, volume);
                    if(alpha085 != null)
                        alpha085.setVolume(volume, volume);
                    if(alpha090 != null)
                        alpha090.setVolume(volume, volume);
                    if(alpha100 != null)
                        alpha100.setVolume(volume, volume);
                    if(alpha105 != null)
                        alpha105.setVolume(volume, volume);
                    if(beta140 != null)
                        beta140.setVolume(volume, volume);
                    if(beta150 != null)
                        beta150.setVolume(volume, volume);
                    if(beta180 != null)
                        beta180.setVolume(volume, volume);
                    if(beta200 != null)
                        beta200.setVolume(volume, volume);
                }

                MediaPlayerManager.getInstance().setVolume(volume);
            }
        });

        btnFrequencia1.setOnClickListener(new View.OnClickListener()        {
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "delta005");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(delta005 == null) {
                    delta005 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta005);
                    delta005.setLooping(true);
                    delta005.setVolume(volume, volume);
                }

                if (btnFrequencia1.isChecked()) {
                    silenciar();
                    btnFrequencia1.setChecked(true);
                    delta005.start();
                    bPlaying = true;
                } else {
                    delta005.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "delta015");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(delta015 == null) {
                    delta015 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta015);
                    delta015.setLooping(true);
                    delta015.setVolume(volume, volume);
                }

                if (btnFrequencia3.isChecked()) {
                    silenciar();
                    btnFrequencia3.setChecked(true);
                    delta015.start();
                    bPlaying = true;
                } else {
                    delta015.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "delta030");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(delta030 == null) {
                    delta030 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta030);
                    delta030.setLooping(true);
                    delta030.setVolume(volume, volume);
                }

                if (btnFrequencia5.isChecked()) {
                    silenciar();
                    btnFrequencia5.setChecked(true);
                    delta030.start();
                    bPlaying = true;
                } else {
                    delta030.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia10.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "theta075");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(theta075 == null) {
                    theta075 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.theta075);
                    theta075.setLooping(true);
                    theta075.setVolume(volume, volume);
                }

                if (btnFrequencia10.isChecked()) {
                    silenciar();
                    btnFrequencia10.setChecked(true);
                    theta075.start();
                    bPlaying = true;
                } else {
                    theta075.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia13.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "alpha100");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(alpha100 == null) {
                    alpha100 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.alpha100);
                    alpha100.setLooping(true);
                    alpha100.setVolume(volume, volume);
                }

                if (btnFrequencia13.isChecked()) {
                    silenciar();
                    btnFrequencia13.setChecked(true);
                    alpha100.start();
                    bPlaying = true;
                } else {
                    alpha100.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia14.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "alpha105");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(alpha105 == null) {
                    alpha105 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.alpha105);
                    alpha105.setLooping(true);
                    alpha105.setVolume(volume, volume);
                }

                if (btnFrequencia14.isChecked()) {
                    silenciar();
                    btnFrequencia14.setChecked(true);
                    alpha105.start();
                    bPlaying = true;
                } else {
                    alpha105.pause();
                    bPlaying = false;
                }
            }
        });

        btnFrequencia18.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                //bundleFirebaseAnalitics.clear();
                //bundleFirebaseAnalitics.putString("frequencia", "beta200");
                //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                if(beta200 == null) {
                    beta200 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.beta200);
                    beta200.setLooping(true);
                    beta200.setVolume(volume, volume);
                }

                if (btnFrequencia18.isChecked()) {
                    silenciar();
                    btnFrequencia18.setChecked(true);
                    beta200.start();
                    bPlaying = true;
                } else {
                    beta200.pause();
                    bPlaying = false;
                }
            }
        });

        if (globalMethods.FREE) {
            //Propagandas
            /*
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            //
            */


            llfrequencia2FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia2_FREE);
            llfrequencia4FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia4_FREE);
            llfrequencia6FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia6_FREE);
            llfrequencia7FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia7_FREE);
            llfrequencia8FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia8_FREE);
            llfrequencia9FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia9_FREE);
            llfrequencia11FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia11_FREE);
            llfrequencia12FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia12_FREE);
            llfrequencia15FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia15_FREE);
            llfrequencia16FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia16_FREE);
            llfrequencia17FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia17_FREE);

            llfrequencia2FREE.setVisibility(View.VISIBLE);
            llfrequencia4FREE.setVisibility(View.VISIBLE);
            llfrequencia6FREE.setVisibility(View.VISIBLE);
            llfrequencia7FREE.setVisibility(View.VISIBLE);
            llfrequencia8FREE.setVisibility(View.VISIBLE);
            llfrequencia9FREE.setVisibility(View.VISIBLE);
            llfrequencia11FREE.setVisibility(View.VISIBLE);
            llfrequencia12FREE.setVisibility(View.VISIBLE);
            llfrequencia15FREE.setVisibility(View.VISIBLE);
            llfrequencia16FREE.setVisibility(View.VISIBLE);
            llfrequencia17FREE.setVisibility(View.VISIBLE);

        } else{
            llfrequencia2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia2);
            llfrequencia4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia4);
            llfrequencia6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia6);
            llfrequencia7 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia7);
            llfrequencia8 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia8);
            llfrequencia9 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia9);
            llfrequencia11 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia11);
            llfrequencia12 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia12);
            llfrequencia15 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia15);
            llfrequencia16 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia16);
            llfrequencia17 = (LinearLayout) findViewById(br.com.relaxapp.R.id.frequencia17);

            llfrequencia2.setVisibility(View.VISIBLE);
            llfrequencia4.setVisibility(View.VISIBLE);
            llfrequencia6.setVisibility(View.VISIBLE);
            llfrequencia7.setVisibility(View.VISIBLE);
            llfrequencia8.setVisibility(View.VISIBLE);
            llfrequencia9.setVisibility(View.VISIBLE);
            llfrequencia11.setVisibility(View.VISIBLE);
            llfrequencia12.setVisibility(View.VISIBLE);
            llfrequencia15.setVisibility(View.VISIBLE);
            llfrequencia16.setVisibility(View.VISIBLE);
            llfrequencia17.setVisibility(View.VISIBLE);


            btnFrequencia2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "delta010");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(delta010 == null) {
                        delta010 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta010);
                        delta010.setLooping(true);
                        delta010.setVolume(volume, volume);
                    }

                    if (btnFrequencia2.isChecked()) {
                        silenciar();
                        btnFrequencia2.setChecked(true);
                        delta010.start();
                        bPlaying = true;
                    } else {
                        delta010.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "delta025");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(delta025 == null) {
                        delta025 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta025);
                        delta025.setLooping(true);
                        delta025.setVolume(volume, volume);
                    }

                    if (btnFrequencia4.isChecked()) {
                        silenciar();
                        btnFrequencia4.setChecked(true);
                        delta025.start();
                        bPlaying = true;
                    } else {
                        delta025.pause();
                        bPlaying = false;
                    }
                }
            });


            btnFrequencia6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "delta035");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(delta035 == null) {
                        delta035 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta035);
                        delta035.setLooping(true);
                        delta035.setVolume(volume, volume);
                    }

                    if (btnFrequencia6.isChecked()) {
                        silenciar();
                        btnFrequencia6.setChecked(true);
                        delta035.start();
                        bPlaying = true;
                    } else {
                        delta035.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "delta040");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(delta040 == null) {
                        delta040 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.delta040);
                        delta040.setLooping(true);
                        delta040.setVolume(volume, volume);
                    }

                    if (btnFrequencia7.isChecked()) {
                        silenciar();
                        btnFrequencia7.setChecked(true);
                        delta040.start();
                        bPlaying = true;
                    } else {
                        delta040.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "theta050");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(theta050 == null) {
                        theta050 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.theta050);
                        theta050.setLooping(true);
                        theta050.setVolume(volume, volume);
                    }

                    if (btnFrequencia8.isChecked()) {
                        silenciar();
                        btnFrequencia8.setChecked(true);
                        theta050.start();
                        bPlaying = true;
                    } else {
                        theta050.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "theta070");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(theta070 == null) {
                        theta070 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.theta070);
                        theta070.setLooping(true);
                        theta070.setVolume(volume, volume);
                    }

                    if (btnFrequencia9.isChecked()) {
                        silenciar();
                        btnFrequencia9.setChecked(true);
                        theta070.start();
                        bPlaying = true;
                    } else {
                        theta070.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "alpha085");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(alpha085 == null) {
                        alpha085 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.alpha085);
                        alpha085.setLooping(true);
                        alpha085.setVolume(volume, volume);
                    }

                    if (btnFrequencia11.isChecked()) {
                        silenciar();
                        btnFrequencia11.setChecked(true);
                        alpha085.start();
                        bPlaying = true;
                    } else {
                        alpha085.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "alpha090");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(alpha090 == null) {
                        alpha090 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.alpha090);
                        alpha090.setLooping(true);
                        alpha090.setVolume(volume, volume);
                    }

                    if (btnFrequencia12.isChecked()) {
                        silenciar();
                        btnFrequencia12.setChecked(true);
                        alpha090.start();
                        bPlaying = true;
                    } else {
                        alpha090.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia15.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "beta140");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(beta140 == null) {
                        beta140 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.beta140);
                        beta140.setLooping(true);
                        beta140.setVolume(volume, volume);
                    }

                    if (btnFrequencia15.isChecked()) {
                        silenciar();
                        btnFrequencia15.setChecked(true);
                        beta140.start();
                        bPlaying = true;
                    } else {
                        beta140.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia16.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "beta150");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(beta150 == null) {
                        beta150 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.beta150);
                        beta150.setLooping(true);
                        beta150.setVolume(volume, volume);
                    }

                    if (btnFrequencia16.isChecked()) {
                        silenciar();
                        btnFrequencia16.setChecked(true);
                        beta150.start();
                        bPlaying = true;
                    } else {
                        beta150.pause();
                        bPlaying = false;
                    }
                }
            });

            btnFrequencia17.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //bundleFirebaseAnalitics.clear();
                    //bundleFirebaseAnalitics.putString("frequencia", "beta180");
                    //firebaseAnalytics.logEvent("frequencias_start", bundleFirebaseAnalitics);

                    if(beta180 == null) {
                        beta180 = MediaPlayer.create(app_frequencias.this, br.com.relaxapp.R.raw.beta180);
                        beta180.setLooping(true);
                        beta180.setVolume(volume, volume);
                    }

                    if (btnFrequencia17.isChecked()) {
                        silenciar();
                        btnFrequencia17.setChecked(true);
                        beta180.start();
                        bPlaying = true;
                    } else {
                        beta180.pause();
                        bPlaying = false;
                    }
                }
            });
        }
    }


    private void silenciar() {
        if ((delta005 != null) && (delta005.isPlaying()))
            delta005.pause();
        if ((delta015 != null) && (delta015.isPlaying()))
            delta015.pause();
        if ((delta030 != null) && (delta030.isPlaying()))
            delta030.pause();
        if ((theta075 != null) && (theta075.isPlaying()))
            theta075.pause();
        if ((alpha100 != null) && (alpha100.isPlaying()))
            alpha100.pause();
        if ((alpha105 != null) && (alpha105.isPlaying()))
            alpha105.pause();
        if ((beta140 != null) && (beta140.isPlaying()))
            beta140.pause();
        if ((beta150 != null) && (beta150.isPlaying()))
            beta150.pause();
        if ((beta180 != null) && (beta180.isPlaying()))
            beta180.pause();
        if ((beta200 != null) && (beta200.isPlaying()))
            beta200.pause();

        btnFrequencia1.setChecked(false);
        btnFrequencia3.setChecked(false);
        btnFrequencia5.setChecked(false);
        btnFrequencia10.setChecked(false);
        btnFrequencia13.setChecked(false);
        btnFrequencia14.setChecked(false);
        btnFrequencia18.setChecked(false);

        if (!globalMethods.FREE) {
            if ((delta010 != null) && (delta010.isPlaying()))
                delta010.pause();
            if ((delta025 != null) && (delta025.isPlaying()))
                delta025.pause();
            if ((delta040 != null) && (delta040.isPlaying()))
                delta040.pause();
            if ((delta035 != null) && (delta035.isPlaying()))
                delta035.pause();
            if ((theta050 != null) && (theta050.isPlaying()))
                theta050.pause();
            if ((theta070 != null) && (theta070.isPlaying()))
                theta070.pause();
            if ((alpha085 != null) && (alpha085.isPlaying()))
                alpha085.pause();
            if ((alpha090 != null) && (alpha090.isPlaying()))
                alpha090.pause();

            btnFrequencia2.setChecked(false);
            btnFrequencia4.setChecked(false);
            btnFrequencia6.setChecked(false);
            btnFrequencia7.setChecked(false);
            btnFrequencia8.setChecked(false);
            btnFrequencia9.setChecked(false);
            btnFrequencia11.setChecked(false);
            btnFrequencia12.setChecked(false);
            btnFrequencia15.setChecked(false);
            btnFrequencia16.setChecked(false);
            btnFrequencia17.setChecked(false);
        }

        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();
    }

    @Override
    public void onBackPressed() {
        voltar();
    }

    public void voltar() {
        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();

        if(bPlaying) {
            intent = new Intent(app_frequencias.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else
            finish();
    }

    @Override
    protected void onStop() {
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;

            if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
            }
        }
        super.onStop();
    }

    public void llClick(View view) {
        if (view.getId() == br.com.relaxapp.R.id.frequencia1)
            btnFrequencia1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia2)
            btnFrequencia2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia3)
            btnFrequencia3.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia4)
            btnFrequencia4.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia5)
            btnFrequencia5.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia6)
            btnFrequencia6.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia7)
            btnFrequencia7.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia8)
            btnFrequencia8.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia9)
            btnFrequencia9.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia10)
            btnFrequencia10.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia11)
            btnFrequencia11.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia12)
            btnFrequencia12.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia13)
            btnFrequencia13.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia14)
            btnFrequencia14.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia15)
            btnFrequencia15.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia16)
            btnFrequencia16.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia17)
            btnFrequencia17.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.frequencia18)
            btnFrequencia18.performClick();
        else
            globalMethods.mostrarPRO();
    }
}