package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.*;
import android.database.*;
import android.os.Bundle;
import android.os.Build;
import android.provider.Settings;

public class SplashScreenActivity extends Activity {
    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;
    Intent intent;

    String sqlConsulta = "SELECT * FROM CONFIGURACOES";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBack));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSplash));
        }

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.activity_splash_screen);


        try {
            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlConsulta, null);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    intent = new Intent();

                    if (cursor.getCount() > 0) {
                        intent.setClass(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);

                        //Permissão para Botão de Pânico
                        cursor.moveToFirst();

                        if(cursor.getString(cursor.getColumnIndex("panico")).trim().equals("S")) {
                            chamarBotaoPanico();
                        }
                        //
                    } else {
                        intent.setClass(SplashScreenActivity.this, app_primeiroacesso.class);
                        startActivity(intent);
                    }

                    finish();
                }
            }, 1500);

        } catch (SQLException ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage(br.com.relaxapp.R.string.erro_BD);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        }
    }


    protected void chamarBotaoPanico(){
        //Permissão para Botão de Pânico
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(SplashScreenActivity.this)) {
            ContentValues data=new ContentValues();
            data.put("panico","N");
            conn.update("CONFIGURACOES", data, null, null);
        } else {
            startService(new Intent(SplashScreenActivity.this, FloatWidgetService.class));
        }
    }
}
