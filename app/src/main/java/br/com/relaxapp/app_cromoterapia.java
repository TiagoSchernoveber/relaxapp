package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 * */

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;


public class app_cromoterapia extends Activity {
    GlobalMethods globalMethods;

    Intent intent;

    int iBrilhoAnterior, iBrilhoAutomatico;
    boolean bAutorizandoConfiguracoes = false;


    LinearLayout llBrilho;
    LinearLayout inicio;
    LinearLayout tbPrincipal;
    LinearLayout efeito;
    ValueAnimator corAnimada;
    TranslateAnimation animateShow;
    TranslateAnimation animateHide;

    Button btnInicio1;
    Button btnInicio2;
    Button btnInicio3;
    Button btnInicio4;
    Button btnInicio5;
    Button btnInicio6;
    Button btnInicio7;

    //private LinearLayout layAdView;
    //AdView mAdView;

    //private FirebaseAnalytics firebaseAnalytics;
    //private Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCromoterapia));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCromoterapia));
        }

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_cromoterapia);

        globalMethods = new GlobalMethods(this);

        /*
        if(globalMethods.FREE) {
            //Propagandas
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);

            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            //
        }
        */

        //Não Bloquear Tela
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ImageView backCromoterapia = (ImageView) findViewById(br.com.relaxapp.R.id.backCromoterapia);
        backCromoterapia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });


        //Efeito Show/Hide barra principal
        tbPrincipal = (LinearLayout) findViewById(br.com.relaxapp.R.id.toolbar);
        animateHide = new TranslateAnimation(0, 0, 0, -300);
        animateHide.setDuration(1000);
        animateHide.setFillAfter(true);
        animateShow = new TranslateAnimation(0, 0, -300, 0);
        animateShow.setDuration(1000);
        animateShow.setFillAfter(true);

        llBrilho = (LinearLayout) findViewById(br.com.relaxapp.R.id.llBrilho);

        setarVisibilidadeBtnBrilhoAutomatico();

        //Views
        inicio = (LinearLayout) findViewById(br.com.relaxapp.R.id.llCromoterapia);
        efeito = (LinearLayout) findViewById(br.com.relaxapp.R.id.efeito);

        //Botões
        btnInicio1 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio1);
        btnInicio2 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio2);
        btnInicio3 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio3);
        btnInicio4 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio4);
        btnInicio5 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio5);
        btnInicio6 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio6);
        btnInicio7 = (Button) findViewById(br.com.relaxapp.R.id.btnInicio7);


        llBrilho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bAutorizandoConfiguracoes = true;
                Intent intentConfig = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intentConfig.setData(Uri.parse("package:" + getPackageName()));
                intentConfig.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentConfig);
            }
        });

        btnInicio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(255, 0, 0), Color.rgb(180, 0, 0));
                aumentaBrilho();
            }
        });

        btnInicio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(255, 128, 0), Color.rgb(255, 180, 0));
                aumentaBrilho();
            }
        });

        btnInicio3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(255, 255, 0), Color.rgb(255, 210, 0));
                aumentaBrilho();
            }
        });

        btnInicio4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(0, 255, 0), Color.rgb(0, 180, 0));
                aumentaBrilho();
            }
        });

        btnInicio5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(55, 110, 230), Color.rgb(0, 70, 220));
                aumentaBrilho();
            }
        });

        btnInicio6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(130, 0, 230), Color.rgb(100, 0, 200));
                aumentaBrilho();
            }
        });

        btnInicio7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                start(Color.rgb(130, 0, 130), Color.rgb(110, 0, 110));
                aumentaBrilho();
            }
        });


        efeito = (LinearLayout) findViewById(br.com.relaxapp.R.id.efeito);

        efeito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                stop();
            }
        });

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();
    }

    private void setarVisibilidadeBtnBrilhoAutomatico() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            llBrilho.setVisibility(View.INVISIBLE);
        else

        {
            if (Settings.System.canWrite(getApplicationContext()))
                llBrilho.setVisibility(View.INVISIBLE);
            else
                llBrilho.setVisibility(View.VISIBLE);
        }

    }

    private void start(int corInicio, int CorFim) {
        //bundleFirebaseAnalitics.clear();
        //bundleFirebaseAnalitics.putInt("icor", corInicio);
        //firebaseAnalytics.logEvent("cromoterapia_start", bundleFirebaseAnalitics);


        inicio.setVisibility(LinearLayout.INVISIBLE);

        corAnimada = ValueAnimator.ofObject(new ArgbEvaluator(), corInicio, CorFim, corInicio);

        corAnimada.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                efeito.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });

        corAnimada.setDuration(1500);

        corAnimada.setRepeatCount(ValueAnimator.INFINITE);

        corAnimada.start();

        efeito.setVisibility(LinearLayout.VISIBLE);

        tbPrincipal.startAnimation(animateHide);
        tbPrincipal.setVisibility(LinearLayout.INVISIBLE);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(corInicio);
            getWindow().setStatusBarColor(corInicio);
        }
    }


    private void stop() {
        reduzirBrilho();

        tbPrincipal.setVisibility(LinearLayout.VISIBLE);
        tbPrincipal.startAnimation(animateShow);

        corAnimada.cancel();

        efeito.setVisibility(LinearLayout.INVISIBLE);

        inicio.setVisibility(LinearLayout.VISIBLE);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCromoterapia));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCromoterapia));
        }

        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();
    }

    @Override
    public void onBackPressed() {
        if (inicio.getVisibility() == View.INVISIBLE)
            stop();
        else
            finish();
    }

    @Override
    protected void onStop() {
        if (!bAutorizandoConfiguracoes) {
            Context context = getApplicationContext();
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

            if (!taskInfo.isEmpty()) {
                ComponentName topActivity = taskInfo.get(0).topActivity;

                if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                    finishAffinity();
                    android.os.Process.killProcess(android.os.Process.myPid());
                    //System.exit(0);
                }
            }
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        bAutorizandoConfiguracoes = false;
        setarVisibilidadeBtnBrilhoAutomatico();
        super.onResume();
    }

    public void aumentaBrilho() {
        try {
            iBrilhoAnterior = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        iBrilhoAutomatico = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        try {
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);  //this will set the manual mode (set the automatic mode off)
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 255);  //this will set the brightness to maximum (255)

            //refreshes the screen
            int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float) br / 255;
            getWindow().setAttributes(lp);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void reduzirBrilho() {
        try {
            if (iBrilhoAutomatico == 1)
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);

            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, iBrilhoAnterior);  //this will set the brightness to maximum (255)

            //refreshes the screen
            int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float) br / 255;
            getWindow().setAttributes(lp);

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void llClick(View view) {
        if (view.getId() == br.com.relaxapp.R.id.llVermelho)
            btnInicio1.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llLaranja)
            btnInicio2.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAmarelo)
            btnInicio3.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llVerde)
            btnInicio4.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llAzul)
            btnInicio5.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llIndigo)
            btnInicio6.performClick();
        else if (view.getId() == br.com.relaxapp.R.id.llVioleta)
            btnInicio7.performClick();

    }
}