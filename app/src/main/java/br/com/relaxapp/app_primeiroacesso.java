package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.database.sqlite.*;
import android.database.*;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Locale;
//import com.google.android.gms.auth.api.signin;

public class app_primeiroacesso extends Activity {
    GlobalMethods globalMethods;
    Intent intent;
    private DataBase dataBase;
    private SQLiteDatabase conn;
    //private Cursor cursor;
    AlertDialog.Builder dlg;

    LinearLayout inicio1;
    EditText edtNome;
    LinearLayout llPanico;
    Switch swPanico;
    TextView tvPanicoStatus;
    //Button btnAvancar1;

    //LinearLayout inicio2;
    //Button btnAvancar2;

    //LinearLayout inicio3;
    //TextView txtNrNotificacoes;
    //SeekBar sbNotificacoes;
    //CheckBox cbDom;
    //CheckBox cbSeg;
    //CheckBox cbTer;
    //CheckBox cbQua;
    //CheckBox cbQui;
    //CheckBox cbSex;
    //CheckBox cbSab;
    //EditText edtNotificacoesInicioHora;
    //EditText edtNotificacoesInicioMinuto;
    //EditText edtNotificacoesFimHora;
    //EditText edtNotificacoesFimMinuto;

    Button btnSalvar;

    //Botão Pânico
    private static final int APP_PERMISSION_REQUEST = 102;

    int horaIni, minIni, horaFim, minFim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBack));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSplash));
        }

        super.onCreate(savedInstanceState);

        setContentView(br.com.relaxapp.R.layout.app_primeiroacesso);

        globalMethods = new GlobalMethods(this);

        inicio1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio1);
        edtNome = (EditText) findViewById(br.com.relaxapp.R.id.edtNome);
        //sbNotificacoes = (SeekBar) findViewById(br.com.relaxapp.R.id.sbNotificacoes);
        llPanico = (LinearLayout) findViewById(br.com.relaxapp.R.id.llPanico);
        swPanico = (Switch) findViewById(br.com.relaxapp.R.id.swPanico);
        tvPanicoStatus = (TextView) findViewById(br.com.relaxapp.R.id.tvPanicoStatus);
        //btnAvancar1 = (Button) findViewById(br.com.relaxapp.R.id.btnAvancar1);

        //inicio2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio2);
        //btnAvancar2 = (Button) findViewById(br.com.relaxapp.R.id.btnAvancar2);

        //inicio3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio3);
        //txtNrNotificacoes = (TextView) findViewById(br.com.relaxapp.R.id.txtNrNotificacoes);
        //cbDom = (CheckBox) findViewById(br.com.relaxapp.R.id.cbDom);
        //cbSeg = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSeg);
        //cbTer = (CheckBox) findViewById(br.com.relaxapp.R.id.cbTer);
        //cbQua = (CheckBox) findViewById(br.com.relaxapp.R.id.cbQua);
        //cbQui = (CheckBox) findViewById(br.com.relaxapp.R.id.cbQui);
        //cbSex = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSex);
        //cbSab = (CheckBox) findViewById(br.com.relaxapp.R.id.cbSab);
        //cbDom = (CheckBox) findViewById(br.com.relaxapp.R.id.cbDom);
        //edtNotificacoesInicioHora = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesInicioHora);
        //edtNotificacoesInicioMinuto = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesInicioMinuto);
        //edtNotificacoesFimHora = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesFimHora);
        //edtNotificacoesFimMinuto = (EditText) findViewById(br.com.relaxapp.R.id.edtNotificacoesFimMinuto);
        btnSalvar = (Button) findViewById(br.com.relaxapp.R.id.btnSalvar);

        llPanico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swPanico.performClick();
            }
        });

        swPanico.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvPanicoStatus.setText(getString(br.com.relaxapp.R.string.ativado));
                    chamarBotaoPanico();
                } else {
                    tvPanicoStatus.setText(getString(br.com.relaxapp.R.string.desativado));
                    stopService(new Intent(app_primeiroacesso.this, FloatWidgetService.class));
                }
            }
        });

        /*
        sbNotificacoes.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtNrNotificacoes.setText(String.valueOf(sbNotificacoes.getProgress()));
            }
        });
        */

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                /*
                if (edtNome.getText().toString().trim().length() == 0) {
                    Toast.makeText(app_primeiroacesso.this, getString(br.com.relaxapp.R.string.erro_nome), Toast.LENGTH_SHORT).show();
                    edtNome.requestFocus();
                } else {
                */

                    //horaIni = Integer.parseInt(edtNotificacoesInicioHora.getText().toString());
                    //minIni = Integer.parseInt(edtNotificacoesInicioMinuto.getText().toString());
                    //horaFim = Integer.parseInt(edtNotificacoesFimHora.getText().toString());
                    //minFim = Integer.parseInt(edtNotificacoesFimMinuto.getText().toString());

                    horaIni = 10;
                    minIni = 00;
                    horaFim = 20;
                    minFim = 00;

                    /*
                    if ((edtNotificacoesInicioHora.getText().toString().trim().length() == 0) || (horaIni < 0) || (horaIni > 24)) {
                        Toast.makeText(app_primeiroacesso.this, getString(br.com.relaxapp.R.string.erro_hora), Toast.LENGTH_SHORT).show();
                        edtNotificacoesInicioHora.requestFocus();
                    } else if ((edtNotificacoesInicioMinuto.getText().toString().trim().length() == 0) || (minIni < 0) || (minIni > 59)) {
                        Toast.makeText(app_primeiroacesso.this, getString(br.com.relaxapp.R.string.erro_minuto), Toast.LENGTH_SHORT).show();
                        edtNotificacoesInicioMinuto.requestFocus();
                    } else if ((edtNotificacoesFimHora.getText().toString().trim().length() == 0) || (horaFim < 0) || (horaFim > 24)) {
                        Toast.makeText(app_primeiroacesso.this, getString(br.com.relaxapp.R.string.erro_hora), Toast.LENGTH_SHORT).show();
                        edtNotificacoesFimHora.requestFocus();
                    } else if ((edtNotificacoesFimMinuto.getText().toString().trim().length() == 0) || (minFim < 0) || (minFim > 59)) {
                        Toast.makeText(app_primeiroacesso.this, getString(br.com.relaxapp.R.string.erro_minuto), Toast.LENGTH_SHORT).show();
                        edtNotificacoesFimMinuto.requestFocus();
                    } else {
                    */
                    try {
                        dataBase = new DataBase(app_primeiroacesso.this);
                        conn = dataBase.getWritableDatabase();

                        ContentValues row = new ContentValues();

                        Locale locale = Locale.getDefault();
                        if(locale.getLanguage().equals("pt"))
                            row.put("nome", "Olá");
                        else if(locale.getLanguage().equals("es"))
                            row.put("nome", "¡Hola");
                        else
                            row.put("nome", "Hello");

                            //row.put("nome", edtNome.getText().toString());

                        //row.put("notificacoes", sbNotificacoes.getProgress());

                        row.put("notificacoes", 1);

                        //if (cbDom.isChecked())
                        row.put("notificacao_dom", "S");
                        //else
                        //row.put("notificacao_dom", "N");

                        //if (cbSeg.isChecked())
                        //row.put("notificacao_seg", "S");
                        //else
                        row.put("notificacao_seg", "N");

                        //if (cbTer.isChecked())
                        row.put("notificacao_ter", "S");
                        //else
                        //row.put("notificacao_ter", "N");

                        //if (cbQua.isChecked())
                        //row.put("notificacao_qua", "S");
                        //else
                        row.put("notificacao_qua", "N");

                        //if (cbQui.isChecked())
                        row.put("notificacao_qui", "S");
                        //else
                        //row.put("notificacao_qui", "N");

                        //if (cbSex.isChecked())
                        //row.put("notificacao_sex", "S");
                        //else
                        row.put("notificacao_sex", "N");

                        //if (cbSab.isChecked())
                        row.put("notificacao_sab", "S");
                        //else
                        //row.put("notificacao_sab", "N");

                        row.put("notificacao_hora_inicio", horaIni);

                        row.put("notificacao_min_inicio", minIni);

                        row.put("notificacao_hora_fim", horaFim);

                        row.put("notificacao_min_fim", minFim);

                        if (swPanico.isChecked())
                            row.put("panico", "S");
                        else
                            row.put("panico", "N");

                        conn.insert("CONFIGURACOES", null, row);


                        globalMethods.setarNotificacoes(
                                1,
                                1,
                                10,
                                0,
                                20,
                                true,
                                false,
                                true,
                                false,
                                true,
                                false,
                                true
                        );


                        Toast.makeText(getApplicationContext(), br.com.relaxapp.R.string.mainConfiguracoes_salvas, Toast.LENGTH_LONG).show();

                        intent = new Intent();

                        intent.setClass(app_primeiroacesso.this, MainActivity.class);
                        startActivity(intent);

                        if (swPanico.isChecked())
                            chamarBotaoPanico();

                        app_primeiroacesso.this.finish();

                    } catch (SQLException ex) {
                        dlg = new AlertDialog.Builder(app_primeiroacesso.this);
                        dlg.setMessage(getString(br.com.relaxapp.R.string.erro_BD));
                        dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        dlg.show();
                    } catch (ParseException e) {
                        dlg = new AlertDialog.Builder(app_primeiroacesso.this);
                        dlg.setMessage(getString(br.com.relaxapp.R.string.erro_BD));
                        dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        dlg.show();
                    }


            }
        });
        btnSalvar.performClick();
    }


    protected void chamarBotaoPanico() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(app_primeiroacesso.this))
            startService(new Intent(app_primeiroacesso.this, FloatWidgetService.class));
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(app_primeiroacesso.this)) {
            swPanico.setChecked(false);
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, APP_PERMISSION_REQUEST);
            Toast.makeText(getApplicationContext(), getString(br.com.relaxapp.R.string.mainConfiguracoes_botaoPanicoAutorizacao), Toast.LENGTH_LONG).show();
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
            startService(new Intent(app_primeiroacesso.this, FloatWidgetService.class));
        else
            Toast.makeText(this, getString(br.com.relaxapp.R.string.erro_panico), Toast.LENGTH_SHORT).show();
    }
}