package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.google.firebase.analytics.FirebaseAnalytics;


public class app_pro extends Activity {
    private FirebaseAnalytics firebaseAnalytics;
    private Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorBarSplash));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarSplash));
        }

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bundleFirebaseAnalitics = new Bundle();

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_pro);
    }

    public void llClick(View view) {
        if (view.getId() == R.id.llClose)
            finish();
        else{
            bundleFirebaseAnalitics.clear();
            firebaseAnalytics.logEvent("pro_start", bundleFirebaseAnalitics);

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.relaxapp.www.relaxapp")));
        }
    }
}
