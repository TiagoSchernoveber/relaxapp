package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class NotificationReceiver extends BroadcastReceiver {
    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;

    @Override
    public void onReceive(Context context, Intent intent){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String diaSemana = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

        String SQLVerifica = "SELECT * FROM CONFIGURACOES";

        try{

            dataBase = new DataBase(context);
            conn = dataBase.getReadableDatabase();

            cursor = conn.rawQuery(SQLVerifica, null);

            cursor.moveToFirst();

            if(     ((diaSemana.equals("Sunday"))    &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_dom")).equals("S")))
                    ||  ((diaSemana.equals("Monday"))    &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_seg")).equals("S")))
                    ||  ((diaSemana.equals("Tuesday"))   &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_ter")).equals("S")))
                    ||  ((diaSemana.equals("Wednesday")) &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_qua")).equals("S")))
                    ||  ((diaSemana.equals("Thursday"))  &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_qui")).equals("S")))
                    ||  ((diaSemana.equals("Friday"))    &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_sex")).equals("S")))
                    ||  ((diaSemana.equals("Saturday"))  &&(cursor.getString(cursor.getColumnIndexOrThrow("notificacao_sab")).equals("S"))) ) {

                Notification.generateNotification(context);

            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
