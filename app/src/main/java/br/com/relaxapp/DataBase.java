package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {

    String SQLCreate1 =     "CREATE TABLE IF NOT EXISTS CONFIGURACOES(                                      " +
                             "  nome                        VARCHAR(15),                                    " +
                            "   notificacoes                INTEGER,                                        " +
                            "   notificacao_dom             CHAR,                                           " +
                            "   notificacao_seg             CHAR,                                           " +
                            "   notificacao_ter             CHAR,                                           " +
                            "   notificacao_qua             CHAR,                                           " +
                            "   notificacao_qui             CHAR,                                           " +
                            "   notificacao_sex             CHAR,                                           " +
                            "   notificacao_sab             CHAR,                                           " +
                            "   notificacao_hora_inicio     INTEGER,                                        " +
                            "   notificacao_min_inicio      INTEGER,                                        " +
                            "   notificacao_hora_fim        INTEGER,                                        " +
                            "   notificacao_min_fim         INTEGER,                                        " +
                            "   panico                      CHAR                                            " +
                            ")                                                                              ";


    String SQLCreate2 =     "CREATE TABLE IF NOT EXISTS CHECKIN(                                            " +
                            "   ansiedade               INTEGER,                                            " +
                            "   exercicio               INTEGER,                                            " +
                            "   resultado               INTEGER,                                            " +
                            "   data                    DATETIME                                            " +
                            ")                                                                              ";


    String SQLCreate3 =     "CREATE TABLE IF NOT EXISTS LOG(                                                " +
                            "   relaxapp               INTEGER,                                             " +
                            "   interticial            INTEGER,                                             " +
                            "   avaliar                INTEGER                                              " +
                            ")                                                                              ";


    public DataBase(Context context){
        super(context, "RelaxApp", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLCreate1);
        db.execSQL(SQLCreate2);
        db.execSQL(SQLCreate3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQLCreate3);
    }
}
