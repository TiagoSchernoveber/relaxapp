package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.content.Context;
import android.media.MediaPlayer;

public class MediaPlayerManager {
    private static MediaPlayerManager manager;

    public static MediaPlayerManager getInstance() {
        if (manager == null) {
            manager = new MediaPlayerManager();
        }
        return manager;
    }

    private MediaPlayer mediaPlayer;

    public void play(Context context, int resId) {
        play(context, resId, null, null);
    }

    public void play(Context context, int resId, MediaPlayer.OnCompletionListener completionListener) {
        play(context, resId, completionListener, null);
    }

    public void play(Context context, int resId, MediaPlayer.OnErrorListener errorListener) {
        play(context, resId, null, errorListener);
    }

    public void play(Context context, int resId, MediaPlayer.OnCompletionListener completionListener, MediaPlayer.OnErrorListener errorListener) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(context.getApplicationContext(), resId);
        mediaPlayer.setOnCompletionListener(completionListener);
        mediaPlayer.setOnErrorListener(errorListener);
        mediaPlayer.start();
    }

    public void stop(Context context, int resId) {
        stop(context, resId, null, null);
    }

    public void stop(Context context, int resId, MediaPlayer.OnCompletionListener completionListener) {
        stop(context, resId, completionListener, null);
    }

    public void stop(Context context, int resId, MediaPlayer.OnErrorListener errorListener) {
        stop(context, resId, null, errorListener);
    }

    public void stop(Context context, int resId, MediaPlayer.OnCompletionListener completionListener, MediaPlayer.OnErrorListener errorListener) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void setVolume(float volume){
        if (mediaPlayer != null)
            mediaPlayer.setVolume(volume, volume);
    }
}
