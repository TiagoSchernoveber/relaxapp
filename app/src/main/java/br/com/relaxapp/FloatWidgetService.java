package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.AlertDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

//import com.google.firebase.analytics.FirebaseAnalytics;

public class FloatWidgetService extends Service {
    private WindowManager mWindowManager;
    private View mFloatingWidget;
    private DataBase dataBase;
    private SQLiteDatabase conn;
    AlertDialog.Builder dlg;

    //private FirebaseAnalytics firebaseAnalytics;

    public FloatWidgetService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mFloatingWidget = LayoutInflater.from(this).inflate(br.com.relaxapp.R.layout.layout_floating_widget, null);

        final WindowManager.LayoutParams params;

        if (Build.VERSION.SDK_INT >= 26)
            params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        else
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = ((width /100) *90);
        params.y = ((height /100) *0);

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingWidget, params);

        ImageView closeButtonCollapsed = (ImageView) mFloatingWidget.findViewById(br.com.relaxapp.R.id.close_btn);

        closeButtonCollapsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopSelf();
            }
        });

        mFloatingWidget.findViewById(br.com.relaxapp.R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            private Intent intentFechar = new Intent(FloatWidgetService.this, FloatWidgetService2.class);

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        startService(intentFechar);

                        return true;
                    case MotionEvent.ACTION_UP:
                        int Xdiff = (int) (event.getRawX() - initialTouchX);
                        int Ydiff = (int) (event.getRawY() - initialTouchY);

                        if ((params.y > ((height /100) *80))) {
                            try {
                                dataBase = new DataBase(FloatWidgetService.this);
                                conn = dataBase.getWritableDatabase();

                                ContentValues row = new ContentValues();

                                row.put("panico", "N");

                                conn.update("CONFIGURACOES", row, null, null);

                                stopSelf();

                            } catch (SQLException ex) {
                                dlg = new AlertDialog.Builder(FloatWidgetService.this);
                                dlg.setMessage(getString(br.com.relaxapp.R.string.erro_BD));
                                dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                dlg.show();
                            }
                        }

                        //onClick
                        int positiveXdiff, positiveYdiff;

                        if(Xdiff < 0)
                            positiveXdiff = Math.abs(Xdiff);
                        else
                            positiveXdiff = Xdiff;

                        if(Ydiff < 0)
                            positiveYdiff = Math.abs(Ydiff);
                        else
                            positiveYdiff = Ydiff;

                        if ((Xdiff == 0 && Ydiff == 0) || (positiveXdiff < 25 && positiveYdiff < 25))  {
                            Toast.makeText(FloatWidgetService.this, getString(br.com.relaxapp.R.string.panico_carregando), Toast.LENGTH_LONG).show();

                            //firebaseAnalytics.logEvent("panico_start", null);

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ComponentName cn = new ComponentName(FloatWidgetService.this, app_checkIn.class);
                            intent.setComponent(cn);

                            intent.putExtra("Mensagem", 1);

                            startActivity(intent);
                        }

                        stopService(intentFechar);

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        mWindowManager.updateViewLayout(mFloatingWidget, params);

                        return true;
                }
                return false;
            }
        });
    }
    private boolean isViewCollapsed() {
        return mFloatingWidget == null || mFloatingWidget.findViewById(br.com.relaxapp.R.id.collapse_view).getVisibility() == View.VISIBLE;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (mFloatingWidget != null) mWindowManager.removeView(mFloatingWidget);
    }
}
