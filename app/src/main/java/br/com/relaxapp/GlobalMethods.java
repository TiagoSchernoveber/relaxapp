package br.com.relaxapp;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.Toast;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Locale;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

public class GlobalMethods {
    Boolean FREE = false;

    Context context;
    AlertDialog.Builder dlg;

    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;

    Integer iRelaxApp, iInterticial, iAvaliar;

    //InterstitialAd mInterstitialAd;
    //AdRequest adRequest;

    MediaPlayer mediaPlayer;

    GlobalMethods() {}

    GlobalMethods(Context context) {
        this.context = context;
        mediaPlayer = new MediaPlayer();

        /*
        if(FREE) {

            adRequest = new AdRequest.Builder().build();

            mInterstitialAd = new InterstitialAd(context);
            mInterstitialAd.setAdUnitId("ca-app-pub-2618106889693283/1581003855");

            if (!mInterstitialAd.isLoaded())
                mInterstitialAd.loadAd(adRequest);
        }
        */
    }

    public void play(String fileName, float volume){
        try {
            FileInputStream fis = context.openFileInput(fileName);
            fis.read();

            mediaPlayer.reset();
            mediaPlayer.setDataSource(fis.getFD());

            fis.close();

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
            mediaPlayer.setVolume(volume, volume);
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        mediaPlayer.reset();
    }

    public void audioReset(){
        Locale locale = Locale.getDefault();

        if (app_mentalizacao.contDownTimer != null) {
            app_mentalizacao.contDownTimer.cancel();
            app_mentalizacao.btnEx1.setChecked(false);
            app_mentalizacao.btnEx2.setChecked(false);
            app_mentalizacao.btnEx3.setChecked(false);
            app_mentalizacao.btnEx4.setChecked(false);
            app_mentalizacao.btnEx5.setChecked(false);

            if(locale.getLanguage().equals("pt")){
                app_mentalizacao.txtContadorEx1.setText("03:54");
                app_mentalizacao.txtContadorEx2.setText("05:27");
                app_mentalizacao.txtContadorEx3.setText("06:13");
                app_mentalizacao.txtContadorEx4.setText("05:46");
                app_mentalizacao.txtContadorEx5.setText("04:09");
            } else {
                app_mentalizacao.txtContadorEx1.setText("04:56");
                app_mentalizacao.txtContadorEx2.setText("06:03");
                app_mentalizacao.txtContadorEx3.setText("06:14");
                app_mentalizacao.txtContadorEx4.setText("06:28");
                app_mentalizacao.txtContadorEx5.setText("04:19");
            }
        }

        if (app_respiracao.contDownTimer != null) {
            app_respiracao.contDownTimer.cancel();
            app_respiracao.btnEx1.setChecked(false);
            app_respiracao.btnEx2.setChecked(false);
            app_respiracao.btnEx3.setChecked(false);
            app_respiracao.btnEx4.setChecked(false);
            app_respiracao.btnEx5.setChecked(false);

            if(locale.getLanguage().equals("pt")) {
                app_respiracao.txtContadorEx1.setText("02:08");
                app_respiracao.txtContadorEx2.setText("02:36");
                app_respiracao.txtContadorEx3.setText("01:51");
                app_respiracao.txtContadorEx4.setText("01:14");
                app_respiracao.txtContadorEx5.setText("01:31");
            } else {
                app_respiracao.txtContadorEx1.setText("02:54");
                app_respiracao.txtContadorEx2.setText("02:28");
                app_respiracao.txtContadorEx3.setText("02:06");
                app_respiracao.txtContadorEx4.setText("01:24");
                app_respiracao.txtContadorEx5.setText("01:36");
            }
        }

        if (app_meditacao.contDownTimer != null) {
            app_meditacao.contDownTimer.cancel();
            app_meditacao.btnEx1.setChecked(false);
            app_meditacao.btnEx2.setChecked(false);
            app_meditacao.btnEx3.setChecked(false);
            app_meditacao.btnEx4.setChecked(false);
            app_meditacao.btnEx5.setChecked(false);

            if(locale.getLanguage().equals("pt")) {
                app_meditacao.txtContadorEx1.setText("06:00");
                app_meditacao.txtContadorEx2.setText("07:09");
                app_meditacao.txtContadorEx3.setText("06:04");
                app_meditacao.txtContadorEx4.setText("07:18");
                app_meditacao.txtContadorEx5.setText("07:49");
            } else{
                app_meditacao.txtContadorEx1.setText("08:20");
                app_meditacao.txtContadorEx2.setText("04:03");
                app_meditacao.txtContadorEx3.setText("06:37");
                app_meditacao.txtContadorEx4.setText("06:22");
                app_meditacao.txtContadorEx5.setText("06:01");
            }
        }
    }

    protected void setarNotificacoes(int nrNotificacoes, int horaIni, int minIni, int horaFim, int minFim, boolean bDom, boolean bSeg, boolean bTer, boolean bQua, boolean bQui, boolean bSex, boolean bSab) throws ParseException {

        try {
            final String ALARM_SERVICE = "alarm";
            int intervaloNotificacoesSegundos;
            Calendar calendar, calendarAgora;
            PendingIntent pendingIntent;
            Intent intent2;
            AlarmManager alarmManager;

            if(nrNotificacoes > 1)
                intervaloNotificacoesSegundos = ((((horaFim - horaIni) * 60) + ((minFim - minIni))) * 60) / (nrNotificacoes - 1);
            else
                intervaloNotificacoesSegundos = ((((horaFim - horaIni) * 60) + ((minFim - minIni))) * 60);

            if (intervaloNotificacoesSegundos < 0)
                intervaloNotificacoesSegundos = -(intervaloNotificacoesSegundos);

            intent2 = new Intent();
            intent2.setClass(context, NotificationReceiver.class);
            alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

            //Cancelar Todos
            //Do dia
            for (int z = 1; z <= 6; z++) {
                pendingIntent = PendingIntent.getActivity(context, (z + 1), intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                pendingIntent.cancel();
                alarmManager.cancel(pendingIntent);
            }

            //Dos próximos dias
            for (int i = 1; i <= 7; i++) {
                for (int z = 1; z <= 6; z++) {
                    pendingIntent = PendingIntent.getActivity(context, (i * 100) + (z + 1), intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    pendingIntent.cancel();
                    alarmManager.cancel(pendingIntent);
                }
            }

            //Inicia agendamento das Notificações
            //Seta para mesmo dia
            calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, horaIni);
            calendar.set(Calendar.MINUTE, minIni);

            calendarAgora = Calendar.getInstance();
            calendarAgora.setTimeInMillis(System.currentTimeMillis());

            if (((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) && (bDom)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) && (bSeg)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) && (bTer)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) && (bQua)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) && (bQui)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) && (bSex)) ||
                    ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) && (bSab))) {

                if (nrNotificacoes > 0) {
                    for (int z = 1; z <= (nrNotificacoes); z++) {
                        if (z > 1)
                            calendar.add(Calendar.SECOND, intervaloNotificacoesSegundos);

                        if (calendar.getTimeInMillis() >= calendarAgora.getTimeInMillis()) {
                            pendingIntent = PendingIntent.getBroadcast(context, (z + 1), intent2, 0);
                            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        }
                    }
                }
            }


            //Seta para próximos dias
            calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, horaIni);
            calendar.set(Calendar.MINUTE, minIni);
            calendar.add(Calendar.DAY_OF_YEAR, 1);

            for (int i = 1; i <= 7; i++) {
                if (((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) && (bDom)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) && (bSeg)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) && (bTer)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) && (bQua)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) && (bQui)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) && (bSex)) ||
                        ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) && (bSab))) {

                    if (nrNotificacoes > 0) {
                        for (int z = 1; z <= (nrNotificacoes); z++) {
                            if (z > 1)
                                calendar.add(Calendar.SECOND, intervaloNotificacoesSegundos);

                            pendingIntent = PendingIntent.getBroadcast(context, (i * 100) + (z + 1), intent2, 0);
                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmManager.INTERVAL_DAY, pendingIntent);
                        }
                    }
                }

                calendar.add(Calendar.DAY_OF_YEAR, 1);
                calendar.set(Calendar.HOUR_OF_DAY, horaIni);
                calendar.set(Calendar.MINUTE, minIni);
            }
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.erro_notificacoes), Toast.LENGTH_SHORT).show();
        }
    }


    public void mostrarPRO() {
        Intent intent = new Intent(context, app_pro.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    public void mostrarIntersticial() {
        /*
        if(FREE) {
            try {
                String sqlConsultaLog = "SELECT * FROM LOG";


                dataBase = new DataBase(context);
                conn = dataBase.getWritableDatabase();

                cursor = conn.rawQuery(sqlConsultaLog, null);
                cursor.moveToFirst();

                ContentValues row = new ContentValues();

                if (cursor.getCount() == 0) {
                    row.put("relaxapp", 1);
                    row.put("interticial", 1);
                    row.put("avaliar", 0);

                    conn.insert("LOG", null, row);
                } else {

                    iInterticial = cursor.getInt(cursor.getColumnIndex("interticial"));
                    iInterticial++;

                    if (iInterticial >= 5) {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                            row.put("interticial", 0);
                        } else
                            row.put("interticial", iInterticial);
                    } else
                        row.put("interticial", iInterticial);


                    conn.update("LOG", row, null, null);
                }
            } catch (SQLException ex) {
                //Toast.makeText(context, "Erro Intersticial", Toast.LENGTH_SHORT).show();
            }
        }
        */
    }

    public void contabilizarLog() {
        try {
            ContentValues row = new ContentValues();

            String sqlConsultaLog = "SELECT * FROM LOG";

            dataBase = new DataBase(context);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlConsultaLog, null);

            if(cursor.getCount() == 0){
                row.put("relaxapp", 1);
                row.put("interticial", 1);
                row.put("avaliar", 0);

                conn.insert("LOG", null, row);

            } else {

                cursor.moveToFirst();

                iRelaxApp = cursor.getInt(cursor.getColumnIndex("relaxapp"));
                iRelaxApp++;

                iAvaliar  = cursor.getInt(cursor.getColumnIndex("avaliar"));


                if ((iRelaxApp >= 50) && (iAvaliar == 0)) {
                    final ContentValues row2 = new ContentValues();

                    dlg = new AlertDialog.Builder(context);
                    dlg.setMessage("Ao avaliar nosso aplicativo na Google Play você ajuda a divulga-lo! Gostaria de fazer isso agora?");
                    dlg.setNeutralButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            row2.put("avaliar", 1);
                            conn.update("LOG", row2, null, null);

                            if (FREE)
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=br.com.relaxapp")));
                            else
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=br.com.relaxapp.www.relaxapp")));
                        }
                    });

                    dlg.setPositiveButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            row2.put("avaliar", 0);
                            conn.update("LOG", row2, null, null);
                        }
                    });

                    dlg.setNegativeButton("Nunca", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            row2.put("avaliar", -1);
                            conn.update("LOG", row2, null, null);
                        }
                    });

                    dlg.show();
                }

                row.put("relaxapp", iRelaxApp);

                conn.update("LOG", row, null, null);
            }

        } catch (SQLException ex) {
            //Toast.makeText(context, "Erro Contabilizar LOG", Toast.LENGTH_SHORT).show();
        }
    }
}
