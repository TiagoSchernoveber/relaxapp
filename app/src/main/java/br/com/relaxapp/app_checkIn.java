package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.TextView;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class app_checkIn extends Activity {
    GlobalMethods globalMethods;

    Intent intent;

    LinearLayout inicio1;
    LinearLayout inicio2;
    LinearLayout inicio3;

    TextView txtInicio3_skb;
    SeekBar skbInicio3_1;

    Button btnResultado_1;
    Button btnResultado_2;
    Button btnResultado_3;
    Button btnResultado_4;

    Button btnFim_1;
    Button btnFim_2;

    Button btnSeguirExercicio;

    int ultimoExFeito = 0 , ultimaAnsiedade = 0;

    LinearLayout ex_baixo1;
    LinearLayout ex_baixo2;
    LinearLayout ex_baixo3;
    LinearLayout ex_baixo4;
    LinearLayout ex_baixo5;
    LinearLayout ex_baixo6;
    LinearLayout ex_baixo7;
    LinearLayout ex_baixo8;
    LinearLayout ex_baixo9;
    LinearLayout ex_baixo10;
    LinearLayout ex_baixo11;
    LinearLayout ex_baixo12;
    LinearLayout ex_baixo13;
    LinearLayout ex_baixo14;

    LinearLayout ex_medio1;
    LinearLayout ex_medio2;
    LinearLayout ex_medio3;
    LinearLayout ex_medio4;
    LinearLayout ex_medio5;
    LinearLayout ex_medio6;
    LinearLayout ex_medio7;
    LinearLayout ex_medio8;
    LinearLayout ex_medio9;
    LinearLayout ex_medio10;
    LinearLayout ex_medio11;
    LinearLayout ex_medio12;

    LinearLayout ex_alto1;
    LinearLayout ex_alto2;
    LinearLayout ex_alto3;
    LinearLayout ex_alto4;
    LinearLayout ex_alto5;
    LinearLayout ex_alto6;
    LinearLayout ex_alto7;
    LinearLayout ex_alto8;
    LinearLayout ex_alto9;
    LinearLayout ex_alto10;
    LinearLayout ex_alto11;
    LinearLayout ex_alto12;
    LinearLayout ex_alto13;
    LinearLayout ex_alto14;

    LinearLayout resultado;
    LinearLayout fim;

    private DataBase dataBase;
    private SQLiteDatabase conn;
    AlertDialog.Builder dlg;

    Integer iAnsiedade, iSorteio, iResultado;

    //private LinearLayout layAdView, layAdView2, layAdView3;
    //private AdView mAdView, mAdView2, mAdView3;

    //private FirebaseAnalytics firebaseAnalytics;
    //private Bundle bundleFirebaseAnalitics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCheckIn));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarCheckIn));
        }

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_checkin);

        globalMethods = new GlobalMethods(this);

        /*
        if(globalMethods.FREE) {
            //Propagandas
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);
            layAdView2 = (LinearLayout) findViewById(R.id.layAdView2);
            layAdView2.setVisibility(View.VISIBLE);
            layAdView3 = (LinearLayout) findViewById(R.id.layAdView3);
            layAdView3.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            mAdView2 = (AdView) findViewById(R.id.adView2);
            mAdView2.loadAd(globalMethods.adRequest);
            mAdView3 = (AdView) findViewById(R.id.adView3);
            mAdView3.loadAd(globalMethods.adRequest);
            //
        }
        */

        ImageView backCheckIn = (ImageView) findViewById(br.com.relaxapp.R.id.backCheckIn);
        backCheckIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        inicio1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio1);
        inicio2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio2);
        inicio3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.inicio3);

        txtInicio3_skb = (TextView) findViewById(br.com.relaxapp.R.id.txtInicio3_skb);
        skbInicio3_1 = (SeekBar) findViewById(br.com.relaxapp.R.id.skbInicio3_1);

        btnResultado_1 = (Button) findViewById(br.com.relaxapp.R.id.btnResultado_1);
        btnResultado_2 = (Button) findViewById(br.com.relaxapp.R.id.btnResultado_2);
        btnResultado_3 = (Button) findViewById(br.com.relaxapp.R.id.btnResultado_3);
        btnResultado_4 = (Button) findViewById(br.com.relaxapp.R.id.btnResultado_4);

        btnFim_1 = (Button) findViewById(br.com.relaxapp.R.id.btnFim_1);
        btnFim_2 = (Button) findViewById(br.com.relaxapp.R.id.btnFim_2);

        btnSeguirExercicio = (Button) findViewById(br.com.relaxapp.R.id.btnSeguirExercicio);

        ex_baixo1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo1);
        ex_baixo2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo2);
        ex_baixo3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo3);
        ex_baixo4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo4);
        ex_baixo5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo5);
        ex_baixo6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo6);
        ex_baixo7 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo7);
        ex_baixo8 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo8);
        ex_baixo9 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo9);
        ex_baixo10 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo10);
        ex_baixo11 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo11);
        ex_baixo12 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo12);
        ex_baixo13 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo13);
        ex_baixo14 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_baixo14);

        ex_medio1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio1);
        ex_medio2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio2);
        ex_medio3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio3);
        ex_medio4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio4);
        ex_medio5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio5);
        ex_medio6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio6);
        //BAIXO/MÉDIO
        ex_medio7 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio7);
        ex_medio8 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio8);
        ex_medio9 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio9);
        ex_medio10 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio10);
        ex_medio11 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio11);
        ex_medio12 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_medio12);

        ex_alto1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto1);
        ex_alto2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto2);
        ex_alto3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto3);
        ex_alto4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto4);
        ex_alto5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto5);
        ex_alto6 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto6);
        ex_alto7 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto7);
        ex_alto8 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto8);
        ex_alto9 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto9);
        ex_alto10 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto10);
        ex_alto11 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto11);
        ex_alto12 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto12);
        ex_alto13 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto13);
        ex_alto14 = (LinearLayout) findViewById(br.com.relaxapp.R.id.ex_alto14);

        resultado = (LinearLayout) findViewById(br.com.relaxapp.R.id.resultado);

        fim = (LinearLayout) findViewById(br.com.relaxapp.R.id.fim);


        skbInicio3_1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtInicio3_skb.setText(String.valueOf(skbInicio3_1.getProgress()));
            }
        });



        btnSeguirExercicio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                seguirExercicio();
            }
        });


        btnResultado_1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                iResultado = 1;
                finalizarExercicio();
                seguirResultado();
            }
        });

        btnResultado_2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                iResultado = 2;
                finalizarExercicio();
                seguirResultado();
            }
        });

        btnResultado_3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                iResultado = 3;
                finalizarExercicio();
                seguirResultado();
            }
        });

        btnResultado_4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                iResultado = 4;
                finalizarExercicio();
                seguirResultado();
            }
        });


        btnFim_1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                reiniciaExercicio();
            }
        });

        btnFim_2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(globalMethods.FREE)
                    globalMethods.mostrarIntersticial();

                finish();
            }
        });

        //Tipo BroadcastReceiver
        if (getIntent() != null && getIntent().getExtras() != null) {
            Intent sender = getIntent();

            if (sender.getExtras().getInt("Mensagem") == 1) {
                iniciarPanico();
            }
        }

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();
    }


    public void fnInicio1(View view) {
        inicio1.setVisibility(LinearLayout.INVISIBLE);
        inicio2.setVisibility(LinearLayout.VISIBLE);
        //mAdView2.loadAd(adRequest2);
    }

    public void fnInicio2(View view) {
        inicio2.setVisibility(LinearLayout.INVISIBLE);
        inicio3.setVisibility(LinearLayout.VISIBLE);
    }

    public void fnInicio3(View view) {
        inicio3.setVisibility(LinearLayout.INVISIBLE);
        ocultarExercicios();

        iAnsiedade = skbInicio3_1.getProgress();
        iniciarExercicio();
    }

    public void iniciarExercicio() {
        btnSeguirExercicio.setVisibility(LinearLayout.VISIBLE);

        Random rSorteio = new Random();

        //bundleFirebaseAnalitics.clear();
        //bundleFirebaseAnalitics.putInt("iansiedade", iAnsiedade);
        //firebaseAnalytics.logEvent("checkin_start", bundleFirebaseAnalitics);

        if (iAnsiedade <= 3) {
            iSorteio = rSorteio.nextInt(5);

            if ((ultimaAnsiedade == iAnsiedade)&&(ultimoExFeito == iSorteio))
                iniciarExercicio();
            else {
                ultimaAnsiedade = iAnsiedade;
                ultimoExFeito = iSorteio;

                switch (iSorteio) {
                    case 0:
                        ex_baixo1.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 1:
                        ex_baixo2.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 2:
                        ex_baixo3.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 3:
                        ex_baixo4.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 4:
                        ex_baixo5.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 5:
                        ex_baixo6.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 6:
                        ex_baixo7.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 7:
                        ex_baixo8.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 8:
                        ex_baixo9.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 9:
                        ex_baixo10.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 10:
                        ex_baixo11.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 11:
                        ex_baixo12.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 12:
                        ex_baixo13.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 13:
                        ex_baixo14.setVisibility(LinearLayout.VISIBLE);
                        break;
                }
            }
        } else if ((iAnsiedade > 3) && (iAnsiedade < 8)) {
            iSorteio = rSorteio.nextInt(19);

            if ((ultimaAnsiedade == iAnsiedade)&&(ultimoExFeito == iSorteio))
                iniciarExercicio();
            else {
                ultimaAnsiedade = iAnsiedade;
                ultimoExFeito = iSorteio;

                switch (iSorteio) {
                    case 0:
                        ex_baixo7.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 1:
                        ex_baixo8.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 2:
                        ex_baixo9.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 3:
                        ex_baixo10.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 4:
                        ex_baixo11.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 5:
                        ex_baixo12.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 6:
                        ex_baixo13.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 7:
                        ex_baixo14.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 8:
                        ex_medio1.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 9:
                        ex_medio2.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 10:
                        ex_medio3.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 11:
                        ex_medio4.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 12:
                        ex_medio5.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 13:
                        ex_medio6.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 14:
                        ex_medio7.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 15:
                        ex_medio8.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 16:
                        ex_medio9.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 17:
                        ex_medio10.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 18:
                        ex_medio11.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 19:
                        ex_medio12.setVisibility(LinearLayout.VISIBLE);
                        break;
                }
            }
        } else {
            iSorteio = rSorteio.nextInt(13);

            if ((ultimaAnsiedade == iAnsiedade)&&(ultimoExFeito == iSorteio))
                iniciarExercicio();
            else {
                ultimaAnsiedade = iAnsiedade;
                ultimoExFeito = iSorteio;

                switch (iSorteio) {
                    case 0:
                        ex_alto1.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 1:
                        ex_alto2.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 2:
                        ex_alto3.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 3:
                        ex_alto4.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 4:
                        ex_alto5.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 5:
                        ex_alto6.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 6:
                        ex_alto7.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 7:
                        ex_alto8.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 8:
                        ex_alto9.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 9:
                        ex_alto10.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 10:
                        ex_alto11.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 11:
                        ex_alto12.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 12:
                        ex_alto13.setVisibility(LinearLayout.VISIBLE);
                        break;
                    case 13:
                        ex_alto14.setVisibility(LinearLayout.VISIBLE);
                        break;
                }
            }
        }
    }

    public void finalizarExercicio() {
        try {
            dataBase = new DataBase(app_checkIn.this);
            conn = dataBase.getWritableDatabase();

            ContentValues row = new ContentValues();


            row.put("ansiedade", iAnsiedade);

            row.put("exercicio", iSorteio);

            row.put("resultado", iResultado);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm ,a");
            String dataAgora = simpleDateFormat.format(calendar.getTime());

            row.put("data", dataAgora);

            conn.insert("CHECKIN", null, row);

        } catch (SQLException ex) {
            dlg = new AlertDialog.Builder(app_checkIn.this);
            dlg.setMessage(getString(br.com.relaxapp.R.string.erro_BD));
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dlg.show();
        }
    }

    public void iniciarPanico() {
        inicio3.setVisibility(LinearLayout.GONE);
        btnSeguirExercicio.setVisibility(LinearLayout.VISIBLE);

        Random rSorteio = new Random();
        int iSorteio = rSorteio.nextInt(13);

        switch (iSorteio) {
            case 0:
                ex_alto1.setVisibility(LinearLayout.VISIBLE);
                break;
            case 1:
                ex_alto2.setVisibility(LinearLayout.VISIBLE);
                break;
            case 2:
                ex_alto3.setVisibility(LinearLayout.VISIBLE);
                break;
            case 3:
                ex_alto4.setVisibility(LinearLayout.VISIBLE);
                break;
            case 4:
                ex_alto5.setVisibility(LinearLayout.VISIBLE);
                break;
            case 5:
                ex_alto6.setVisibility(LinearLayout.VISIBLE);
                break;
            case 6:
                ex_alto7.setVisibility(LinearLayout.VISIBLE);
                break;
            case 7:
                ex_alto8.setVisibility(LinearLayout.VISIBLE);
                break;
            case 8:
                ex_alto9.setVisibility(LinearLayout.VISIBLE);
                break;
            case 9:
                ex_alto10.setVisibility(LinearLayout.VISIBLE);
                break;
            case 10:
                ex_alto11.setVisibility(LinearLayout.VISIBLE);
                break;
            case 11:
                ex_alto12.setVisibility(LinearLayout.VISIBLE);
                break;
            case 12:
                ex_alto13.setVisibility(LinearLayout.VISIBLE);
                break;
            case 13:
                ex_alto14.setVisibility(LinearLayout.VISIBLE);
                break;
        }

    }

    public void ocultarExercicios() {
        ex_baixo1.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo2.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo3.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo4.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo5.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo6.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo7.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo8.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo9.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo10.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo11.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo12.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo13.setVisibility(LinearLayout.INVISIBLE);
        ex_baixo14.setVisibility(LinearLayout.INVISIBLE);
        ex_medio1.setVisibility(LinearLayout.INVISIBLE);
        ex_medio2.setVisibility(LinearLayout.INVISIBLE);
        ex_medio3.setVisibility(LinearLayout.INVISIBLE);
        ex_medio4.setVisibility(LinearLayout.INVISIBLE);
        ex_medio5.setVisibility(LinearLayout.INVISIBLE);
        ex_medio6.setVisibility(LinearLayout.INVISIBLE);
        ex_medio7.setVisibility(LinearLayout.INVISIBLE);
        ex_medio8.setVisibility(LinearLayout.INVISIBLE);
        ex_medio9.setVisibility(LinearLayout.INVISIBLE);
        ex_medio10.setVisibility(LinearLayout.INVISIBLE);
        ex_medio11.setVisibility(LinearLayout.INVISIBLE);
        ex_medio12.setVisibility(LinearLayout.INVISIBLE);
        ex_alto1.setVisibility(LinearLayout.INVISIBLE);
        ex_alto2.setVisibility(LinearLayout.INVISIBLE);
        ex_alto3.setVisibility(LinearLayout.INVISIBLE);
        ex_alto4.setVisibility(LinearLayout.INVISIBLE);
        ex_alto5.setVisibility(LinearLayout.INVISIBLE);
        ex_alto6.setVisibility(LinearLayout.INVISIBLE);
        ex_alto7.setVisibility(LinearLayout.INVISIBLE);
        ex_alto8.setVisibility(LinearLayout.INVISIBLE);
        ex_alto9.setVisibility(LinearLayout.INVISIBLE);
        ex_alto10.setVisibility(LinearLayout.INVISIBLE);
        ex_alto11.setVisibility(LinearLayout.INVISIBLE);
        ex_alto12.setVisibility(LinearLayout.INVISIBLE);
        ex_alto13.setVisibility(LinearLayout.INVISIBLE);
        ex_alto14.setVisibility(LinearLayout.INVISIBLE);

        btnSeguirExercicio.setVisibility(LinearLayout.INVISIBLE);
    }


    public void reiniciaExercicio() {
        //bundleFirebaseAnalitics.clear();
        //firebaseAnalytics.logEvent("checkin_restart", null);

        ocultarExercicios();

        inicio1.setVisibility(LinearLayout.INVISIBLE);
        inicio2.setVisibility(LinearLayout.INVISIBLE);

        resultado.setVisibility(LinearLayout.INVISIBLE);
        fim.setVisibility(LinearLayout.INVISIBLE);

        inicio3.setVisibility(LinearLayout.VISIBLE);

    }

    public void seguirExercicio() {
        ocultarExercicios();

        resultado.setVisibility(LinearLayout.VISIBLE);
    }

    public void seguirResultado() {
        resultado.setVisibility(LinearLayout.INVISIBLE);

        fim.setVisibility(LinearLayout.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        voltar();
    }

    public void voltar() {
        intent = new Intent(app_checkIn.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();
    }

    @Override
    protected void onStop() {
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;

            if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
            }
        }

        super.onStop();
    }
}
