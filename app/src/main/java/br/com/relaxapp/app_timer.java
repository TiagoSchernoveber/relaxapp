package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.InterstitialAd;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Calendar;
import java.util.List;

public class app_timer extends Activity {
    GlobalMethods globalMethods;

    Intent intent;

    LinearLayout llInicio;
    LinearLayout llContador;
    Button btnIniciar;
    Button btnCancelar;

    TextView txtContador;
    NumberPicker npMinutos;

    CountDownTimer contDownTimer;

    //AdView mAdView;

    //private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarTimer));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarTimer));
        }

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_timer);

        globalMethods = new GlobalMethods(this);

        /*
        if(globalMethods.FREE) {
            //Propagandas
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.setVisibility(View.VISIBLE);
            //
        }
        */

        llInicio = (LinearLayout) findViewById(br.com.relaxapp.R.id.llInicio);
        llContador = (LinearLayout) findViewById(br.com.relaxapp.R.id.llContador);

        btnIniciar = (Button) findViewById(br.com.relaxapp.R.id.btnIniciar);
        btnCancelar = (Button) findViewById(br.com.relaxapp.R.id.btnCancelar);

        txtContador = (TextView) findViewById(br.com.relaxapp.R.id.txtContador);

        ImageView backCheckIn = (ImageView) findViewById(br.com.relaxapp.R.id.backTimer);

        backCheckIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                voltar();
            }
        });

        npMinutos = (NumberPicker) findViewById(br.com.relaxapp.R.id.npMinutos);

        npMinutos.setMinValue(1);
        npMinutos.setMaxValue(60);


        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                llInicio.setVisibility(LinearLayout.INVISIBLE);
                llContador.setVisibility(LinearLayout.VISIBLE);

                //firebaseAnalytics.logEvent("timer_start", null);

                contDownTimer = new CountDownTimer((npMinutos.getValue() *60 *1000), 1000) {

                    public void onTick(long millisUntilFinished) {
                        txtContador.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                    }

                    public void onFinish() {
                        System.exit(1);
                    }

                    public String getCorrectTime(boolean isMinute, long millisUntilFinished){
                        String aux;
                        int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                        Calendar c = Calendar.getInstance();
                        c.setTimeInMillis(millisUntilFinished);

                        aux = c.get(constCalendar) < 10 ? "0"+ c.get(constCalendar) : ""+ c.get(constCalendar);

                        return(aux);
                    }
                }.start();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                llInicio.setVisibility(LinearLayout.VISIBLE);
                llContador.setVisibility(LinearLayout.INVISIBLE);

                contDownTimer.cancel();
            }
        });
    }

    @Override
    public void onBackPressed() {
        voltar();
    }

    public void voltar() {
        intent = new Intent(app_timer.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;

            if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
            }
        }
        super.onStop();
    }
}
