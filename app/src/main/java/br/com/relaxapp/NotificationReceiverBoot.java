package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class NotificationReceiverBoot extends BroadcastReceiver {
    GlobalMethods globalMethods;

    @Override
    public void onReceive(Context context, Intent intent) {
        DataBase dataBase;
        SQLiteDatabase conn;
        Cursor cursor;

        String SQLVerificaConfig = "SELECT * FROM CONFIGURACOES";

        try{
            dataBase = new DataBase(context);
            conn = dataBase.getReadableDatabase();

            cursor = conn.rawQuery(SQLVerificaConfig, null);
            cursor.moveToFirst();

            if(cursor.getCount() > 0) {
                globalMethods = new GlobalMethods();

                globalMethods.setarNotificacoes(
                                                cursor.getInt(cursor.getColumnIndexOrThrow("notificacoes")),
                                                cursor.getInt(cursor.getColumnIndexOrThrow("notificacao_hora_inicio")),
                                                cursor.getInt(cursor.getColumnIndexOrThrow("notificacao_min_inicio")),
                                                cursor.getInt(cursor.getColumnIndexOrThrow("notificacao_hora_fim")),
                                                cursor.getInt(cursor.getColumnIndexOrThrow("notificacao_min_fim")),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_dom")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_seg")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_ter")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_qua")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_qui")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_sex")).equals("S"),
                                                cursor.getString(cursor.getColumnIndexOrThrow("notificacao_sab")).equals("S"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
