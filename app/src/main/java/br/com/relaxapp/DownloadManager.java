package br.com.relaxapp;

/**
 * Created by tiago on 21/07/2018.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Locale;

public class DownloadManager extends AsyncTask<String, String, Boolean> {

    private final Context mContext;
    private final  Activity mActivity;
    ProgressBar progressBar;
    TextView textView;
    String parametroFinal;
    //PTBR_GBdNLDGOa0Pwpz0Ho0tVUPMr40fS8fYs

    public DownloadManager(final Context context, final Activity activity){
        mContext = context;
        mActivity = activity;
    };


    public DownloadManager(final Context context, final Activity activity, View vProgressBar) {
        mContext = context;
        mActivity = activity;
        progressBar = (ProgressBar) vProgressBar;
    }


    private void manipularVisibilidade(final Boolean efetuou, final String elemento){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mContext instanceof app_mentalizacao) {
                    app_mentalizacao.manipularVisibilidade(efetuou, elemento);
                    app_mentalizacao.bDownloading = false;
                    app_mentalizacao.setarFuncaoBotoes();
                } else if(mContext instanceof app_respiracao){
                    app_respiracao.manipularVisibilidade(efetuou, elemento);
                    app_respiracao.bDownloading = false;
                    app_respiracao.setarFuncaoBotoes();
                } else if(mContext instanceof app_meditacao){
                    app_meditacao.manipularVisibilidade(efetuou, elemento);
                    app_meditacao.bDownloading = false;
                    app_meditacao.setarFuncaoBotoes();
                }

                if (!efetuou)
                    Toast.makeText(mContext, mContext.getString(R.string.erro_download), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void manipularProgresso(final Integer progresso){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mContext instanceof app_mentalizacao) {
                    switch (parametroFinal) {
                        case "Ex1":
                            app_mentalizacao.txtPercentEx1.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex2":
                            app_mentalizacao.txtPercentEx2.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex3":
                            app_mentalizacao.txtPercentEx3.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex4":
                            app_mentalizacao.txtPercentEx4.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex5":
                            app_mentalizacao.txtPercentEx5.setText(Integer.toString(progresso) + "%");
                            break;
                    }
                } else if(mContext instanceof app_respiracao){
                        switch (parametroFinal) {
                            case "Ex1":
                                app_respiracao.txtPercentEx1.setText(Integer.toString(progresso) + "%");
                                break;
                            case "Ex2":
                                app_respiracao.txtPercentEx2.setText(Integer.toString(progresso) + "%");
                                break;
                            case "Ex3":
                                app_respiracao.txtPercentEx3.setText(Integer.toString(progresso) + "%");
                                break;
                            case "Ex4":
                                app_respiracao.txtPercentEx4.setText(Integer.toString(progresso) + "%");
                                break;
                            case "Ex5":
                                app_respiracao.txtPercentEx5.setText(Integer.toString(progresso) + "%");
                                break;
                        }
                } else if(mContext instanceof app_meditacao){
                    switch (parametroFinal) {
                        case "Ex1":
                            app_meditacao.txtPercentEx1.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex2":
                            app_meditacao.txtPercentEx2.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex3":
                            app_meditacao.txtPercentEx3.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex4":
                            app_meditacao.txtPercentEx4.setText(Integer.toString(progresso) + "%");
                            break;
                        case "Ex5":
                            app_meditacao.txtPercentEx5.setText(Integer.toString(progresso) + "%");
                            break;
                    }
                }
            }
        });
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... parametro) {
        int count = 0;
        int progresso = 0;

        try {
            parametroFinal = parametro[1];

            Locale locale = Locale.getDefault();

            URL url = null;

            switch (parametro[0]) {
                case "mentalizacao_1":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmentalizacao_1.mp3?alt=media&token=8f59a255-55db-420e-8d2a-89f4d312eb5b");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmentalizacao_1.mp3?alt=media&token=1bd0e0a7-3c08-4e77-97fb-5dc334b5c1a7");
                    break;
                case "mentalizacao_2":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmentalizacao_2.mp3?alt=media&token=4082d044-cae5-4e22-aca5-ba17eb759ba3");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmentalizacao_2.mp3?alt=media&token=c186490c-f941-4e3c-a736-912c11ae5726");
                    break;
                case "mentalizacao_3":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmentalizacao_3.mp3?alt=media&token=901e14c7-dc07-42c4-95fb-3e69de0d37c6");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmentalizacao_3.mp3?alt=media&token=911ee12a-8ebf-4ee2-af50-059a09f3cfff");
                    break;
                case "mentalizacao_4":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmentalizacao_4.mp3?alt=media&token=86313f37-6f28-4564-9c4b-c7a72c9fc006");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmentalizacao_4.mp3?alt=media&token=b3b02a72-6162-4c9b-a6b4-245df9b42b77");
                    break;
                case "mentalizacao_5":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmentalizacao_5.mp3?alt=media&token=4d4fd902-9b00-4e32-b555-f6732a4f91f3");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmentalizacao_5.mp3?alt=media&token=577cba66-3a94-4dc1-9bfa-351738ac683f");
                    break;
                case "respiracao_1":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Frespiracao_1.mp3?alt=media&token=f78a8c34-3f54-4798-b22a-b79607447f0b");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Frespiracao_1.mp3?alt=media&token=2cbd464e-dc53-4621-ae3e-2d0e1ebb2701");
                    break;
                case "respiracao_2":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Frespiracao_2.mp3?alt=media&token=2090a504-9f87-4f96-88bc-7d94451f27f5");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Frespiracao_2.mp3?alt=media&token=dbf0d8a2-069d-46c4-b5c5-e84b1ff07f02");
                    break;
                case "respiracao_3":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Frespiracao_3.mp3?alt=media&token=1fe271e6-3117-4717-94f9-0bb62be3480a");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Frespiracao_3.mp3?alt=media&token=3349147e-d713-4725-9e16-8969e05b9961");
                    break;
                case "respiracao_4":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Frespiracao_4.mp3?alt=media&token=7e785908-194a-45db-b941-f9f9abcad6b4");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Frespiracao_4.mp3?alt=media&token=ccc3dbc2-6e35-486c-aa9b-66e3cb52aee4");
                    break;
                case "respiracao_5":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Frespiracao_5.mp3?alt=media&token=5aa0374a-2279-4820-9316-ef5f6f6ba043");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Frespiracao_5.mp3?alt=media&token=72a12f0b-9d04-4307-8465-de956c33e4a4");
                    break;
                case "meditacao_1":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmeditacao_1.mp3?alt=media&token=b9f641e8-30aa-4c78-8bd6-3562534d0d3b");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmeditacao_1.mp3?alt=media&token=4276a10c-7c5f-4141-bc7b-263b26be438c");
                    break;
                case "meditacao_2":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmeditacao_2.mp3?alt=media&token=bb50b928-83d5-4314-915a-e3092a981607");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmeditacao_2.mp3?alt=media&token=5374a1d6-7a8d-4683-bc96-b2224c73f3ad");
                    break;
                case "meditacao_3":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmeditacao_3.mp3?alt=media&token=1125357e-38a6-4ab8-9f5d-3a50be1d120d");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmeditacao_3.mp3?alt=media&token=1667da48-2d41-42a5-8200-ff213b97caa2");
                    break;
                case "meditacao_4":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmeditacao_4.mp3?alt=media&token=c1c25852-fec7-41a1-b28a-8f6364140e3a");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmeditacao_4.mp3?alt=media&token=59f3d94e-6b11-421f-992f-17a33b382f3a");
                    break;
                case "meditacao_5":
                    if(locale.getLanguage().equals("pt"))
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/PT%2Fmeditacao_5.mp3?alt=media&token=b0929aa9-ca1e-4583-ae6f-a1b20bf5d366");
                    else
                        url = new URL("https://firebasestorage.googleapis.com/v0/b/relaxapp-9af45.appspot.com/o/EN%2Fmeditacao_5.mp3?alt=media&token=214e044d-7d9b-4a5a-a242-12c6f1f1f512");
                    break;
            }



            FileOutputStream fos = mContext.openFileOutput(parametro[0] + ".mp3", Context.MODE_PRIVATE);

            if (Build.VERSION.SDK_INT > 23) {
                progressBar.setProgress(25);
                manipularProgresso(25);
                ReadableByteChannel rbc;
                progressBar.setProgress(50);
                manipularProgresso(50);
                rbc = Channels.newChannel(url.openStream());
                progressBar.setProgress(75);
                manipularProgresso(75);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                rbc.close();

            } else {
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 1024);
                byte data[] = new byte[1];
                long total = 0;


                while ((count = input.read(data)) != -1) {
                    total += count;
                    progresso = (int) ((total * 100) / lenghtOfFile);
                    progressBar.setProgress(progresso);
                    manipularProgresso(progresso);
                    fos.write(data);
                }
                input.close();

            }

            fos.close();

            progressBar.setProgress(0);
        } catch (Exception e) {
            delete(parametro[0]);
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if(mContext instanceof app_mentalizacao) {
            app_mentalizacao.verificar();
        } else if(mContext instanceof app_respiracao){
            app_respiracao.verificar();
        } else if(mContext instanceof app_meditacao) {
            app_meditacao.verificar();
        }

        manipularVisibilidade(result, parametroFinal);
    }

    public boolean verify(String fileName){
        File f = new File(mContext.getFilesDir().getAbsolutePath() +"/" + fileName);
        return f.exists();
    }

    public boolean delete(String fileName){
        File f = new File(mContext.getFilesDir().getAbsolutePath() +"/" + fileName);
        return f.delete();
    }
}
