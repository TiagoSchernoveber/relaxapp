package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

public class Notification {
    @SuppressWarnings("static-access")
    
    public static String createNotificationChannel(Context context) {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String channelId = "Channel_id";

            CharSequence channelName = "RelaxApp";
            // The user-visible description of the channel.
            String channelDescription = "RelaxApp Alert";
            int channelImportance = NotificationManager.IMPORTANCE_DEFAULT;
            boolean channelEnableVibrate = true;
            //            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
            //            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            // Returns null for pre-O (26) devices.
            return null;
        }
    }

    public static void generateNotification(Context context) {
        String SQLVerifica = "SELECT nome FROM CONFIGURACOES";
        DataBase dataBase;
        SQLiteDatabase conn;
        Cursor cursor;
        String nome = "";

        try {
            dataBase = new DataBase(context);
            conn = dataBase.getReadableDatabase();

            cursor = conn.rawQuery(SQLVerifica, null);

            cursor.moveToFirst();
            nome = cursor.getString(cursor.getColumnIndex("nome")).trim();

        } catch (SQLException ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(context);
            dlg.setMessage(br.com.relaxapp.R.string.erro_BD);
            dlg.setNeutralButton("OK", null);
            dlg.show();
        }

        Resources res = context.getResources();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, createNotificationChannel(context))
                .setSmallIcon(br.com.relaxapp.R.drawable.logonotificacao)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), br.com.relaxapp.R.drawable.logo))
                .setContentTitle(res.getText(br.com.relaxapp.R.string.app_name))
                .setContentText(nome + ", " + res.getText(br.com.relaxapp.R.string.notificacao));

        Intent notificationIntent = new Intent(context, app_checkIn.class);
        notificationIntent.putExtra("origem", "notificacao");
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        //Notificação desaparece quando suuário clica nela
        builder.setAutoCancel(true);

        //Vibração
        builder.setVibrate(new long[]{150,300,150,300});

        //Adiciona Notificação
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(0, builder.build());

        //Tocar som Notificação
        try{
            Uri som = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(context, som);
            toque.play();
        }catch (Exception ex){ }


    }
}
