package br.com.relaxapp;

/**
 * Created by Tiago Schernoveber on 25/10/2017.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.MobileAds;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class app_meditacao extends Activity {
    static GlobalMethods globalMethods;
    static DownloadManager downloadManager;

    Intent intent;

    public static CountDownTimer contDownTimer;

    public static SeekBar sbVolume;
    static float volume;

    public static Boolean bPlaying = false;
    public static Boolean bDownloading = false;
    public static Boolean bEx1 = false;
    public static Boolean bEx2 = false;
    public static Boolean bEx3 = false;
    public static Boolean bEx4 = false;
    public static Boolean bEx5 = false;

    LinearLayout llEx1;
    public static ToggleButton btnEx1;
    public static TextView txtContadorEx1;
    static LinearLayout llManageEx1;
    static LinearLayout llManageEx1_2;
    static LinearLayout llManageEx1_3;
    ProgressBar pbEx1;
    public static TextView txtPercentEx1;
    static LinearLayout llDescEx1;

    LinearLayout llEx2;
    public static ToggleButton btnEx2;
    public static TextView txtContadorEx2;
    static LinearLayout llManageEx2;
    static LinearLayout llManageEx2_2;
    static LinearLayout llManageEx2_3;
    ProgressBar pbEx2;
    public static TextView txtPercentEx2;
    static LinearLayout llDescEx2;

    LinearLayout llEx3;
    public static ToggleButton btnEx3;
    public static TextView txtContadorEx3;
    static LinearLayout llManageEx3;
    static LinearLayout llManageEx3_2;
    static LinearLayout llManageEx3_3;
    ProgressBar pbEx3;
    public static TextView txtPercentEx3;
    static LinearLayout llDescEx3;

    LinearLayout llEx3_FREE;

    LinearLayout llEx4;
    public static ToggleButton btnEx4;
    public static TextView txtContadorEx4;
    static LinearLayout llManageEx4;
    static LinearLayout llManageEx4_2;
    static LinearLayout llManageEx4_3;
    ProgressBar pbEx4;
    public static TextView txtPercentEx4;
    static LinearLayout llDescEx4;

    LinearLayout llEx4_FREE;

    LinearLayout llEx5;
    public static ToggleButton btnEx5;
    public static TextView txtContadorEx5;
    static LinearLayout llManageEx5;
    static LinearLayout llManageEx5_2;
    static LinearLayout llManageEx5_3;
    ProgressBar pbEx5;
    public static TextView txtPercentEx5;
    static LinearLayout llDescEx5;

    LinearLayout llEx5_FREE;

    //private LinearLayout layAdView;
    //AdView mAdView;

    //private static FirebaseAnalytics firebaseAnalytics;
    //private static Bundle bundleFirebaseAnalitics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarMeditacao));
            getWindow().setStatusBarColor(getResources().getColor(br.com.relaxapp.R.color.colorBarMeditacao));
        }

        //firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //bundleFirebaseAnalitics = new Bundle();

        super.onCreate(savedInstanceState);
        setContentView(br.com.relaxapp.R.layout.app_meditacao);

        globalMethods = new GlobalMethods(this);
        downloadManager = new DownloadManager(this, this);

        ImageView backCheckIn = (ImageView) findViewById(br.com.relaxapp.R.id.backMeditacao);
        backCheckIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                voltar();
            }
        });

        sbVolume = (SeekBar) findViewById(br.com.relaxapp.R.id.sbVolume);

        llEx1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx1);
        btnEx1 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnEx1);
        txtContadorEx1 = (TextView) findViewById(br.com.relaxapp.R.id.txtContadorEx1);
        llManageEx1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx1);
        llManageEx1_2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx1_2);
        llManageEx1_3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx1_3);
        pbEx1 = (ProgressBar) findViewById(br.com.relaxapp.R.id.pbEx1);
        txtPercentEx1 = (TextView) findViewById(br.com.relaxapp.R.id.txtPercentEx1);
        llDescEx1 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llDescEx1);

        llEx2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx2);
        btnEx2 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnEx2);
        txtContadorEx2 = (TextView) findViewById(br.com.relaxapp.R.id.txtContadorEx2);
        llManageEx2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx2);
        llManageEx2_2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx2_2);
        llManageEx2_3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx2_3);
        pbEx2 = (ProgressBar) findViewById(br.com.relaxapp.R.id.pbEx2);
        txtPercentEx2 = (TextView) findViewById(br.com.relaxapp.R.id.txtPercentEx2);
        llDescEx2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llDescEx2);

        llEx3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx3);
        btnEx3 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnEx3);
        txtContadorEx3 = (TextView) findViewById(br.com.relaxapp.R.id.txtContadorEx3);
        llManageEx3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx3);
        llManageEx3_2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx3_2);
        llManageEx3_3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx3_3);
        pbEx3 = (ProgressBar) findViewById(br.com.relaxapp.R.id.pbEx3);
        txtPercentEx3 = (TextView) findViewById(br.com.relaxapp.R.id.txtPercentEx3);
        llDescEx3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llDescEx3);

        llEx4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx4);
        btnEx4 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnEx4);
        txtContadorEx4 = (TextView) findViewById(br.com.relaxapp.R.id.txtContadorEx4);
        llManageEx4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx4);
        llManageEx4_2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx4_2);
        llManageEx4_3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx4_3);
        pbEx4 = (ProgressBar) findViewById(br.com.relaxapp.R.id.pbEx4);
        txtPercentEx4 = (TextView) findViewById(br.com.relaxapp.R.id.txtPercentEx4);
        llDescEx4 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llDescEx4);

        llEx5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx5);
        btnEx5 = (ToggleButton) findViewById(br.com.relaxapp.R.id.btnEx5);
        txtContadorEx5 = (TextView) findViewById(br.com.relaxapp.R.id.txtContadorEx5);
        llManageEx5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx5);
        llManageEx5_2 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx5_2);
        llManageEx5_3 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llManageEx5_3);
        pbEx5 = (ProgressBar) findViewById(br.com.relaxapp.R.id.pbEx5);
        txtPercentEx5 = (TextView) findViewById(br.com.relaxapp.R.id.txtPercentEx5);
        llDescEx5 = (LinearLayout) findViewById(br.com.relaxapp.R.id.llDescEx5);

        if ((app_mentalizacao.sbVolume != null) && (sbVolume.getProgress() != app_mentalizacao.sbVolume.getProgress()))
            sbVolume.setProgress(app_mentalizacao.sbVolume.getProgress());
        else if ((app_respiracao.sbVolume != null) && (sbVolume.getProgress() != app_respiracao.sbVolume.getProgress()))
            sbVolume.setProgress(app_respiracao.sbVolume.getProgress());

        sbVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volume = ((float) progress / 10);

                MediaPlayerManager.getInstance().setVolume(volume);

                if ((app_mentalizacao.sbVolume != null) && (app_mentalizacao.sbVolume.getProgress() != progress))
                    app_mentalizacao.sbVolume.setProgress(progress);

                if ((app_respiracao.sbVolume != null) && (app_respiracao.sbVolume.getProgress() != progress))
                    app_respiracao.sbVolume.setProgress(progress);
            }
        });

        bEx1 = downloadManager.verify("meditacao_1.mp3");
        bEx2 = downloadManager.verify("meditacao_2.mp3");


        llEx1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bEx1 != null && bEx1)
                    btnEx1.performClick();
                else
                    manipularArquivo(llManageEx1);
            }
        });

        llEx2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bEx2 != null && bEx2)
                    btnEx2.performClick();
                else
                    manipularArquivo(llManageEx2);
            }
        });


        if (globalMethods.FREE) {
            //Propagandas
            /*
            layAdView = (LinearLayout) findViewById(R.id.layAdView);
            layAdView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, "ca-app-pub-2618106889693283/3183417064");
            mAdView = (AdView) findViewById(R.id.adView);
            mAdView.loadAd(globalMethods.adRequest);
            //
            */


            llEx3_FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx3_FREE);
            llEx4_FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx4_FREE);
            llEx5_FREE = (LinearLayout) findViewById(br.com.relaxapp.R.id.llEx5_FREE);

            llEx3_FREE.setVisibility(View.VISIBLE);
            llEx4_FREE.setVisibility(View.VISIBLE);
            llEx5_FREE.setVisibility(View.VISIBLE);

            llEx3_FREE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    globalMethods.mostrarPRO();
                }
            });

            llEx4_FREE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    globalMethods.mostrarPRO();
                }
            });

            llEx5_FREE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    globalMethods.mostrarPRO();
                }
            });
        } else {
            bEx3 = downloadManager.verify("meditacao_3.mp3");
            bEx4 = downloadManager.verify("meditacao_4.mp3");
            bEx5 = downloadManager.verify("meditacao_5.mp3");

            llEx3.setVisibility(View.VISIBLE);
            llEx4.setVisibility(View.VISIBLE);
            llEx5.setVisibility(View.VISIBLE);

            llEx3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bEx3 != null && bEx3)
                        btnEx3.performClick();
                    else
                        manipularArquivo(llManageEx3);
                }
            });

            llEx4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bEx4 != null && bEx4)
                        btnEx4.performClick();
                    else
                        manipularArquivo(llManageEx4);
                }
            });

            llEx5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bEx5 != null && bEx5)
                        btnEx5.performClick();
                    else
                        manipularArquivo(llManageEx5);
                }
            });
        }

        setarFuncaoBotoes();
        manipularVisibilidade(bEx1, "Ex1");
        manipularVisibilidade(bEx2, "Ex2");
        manipularVisibilidade(bEx3, "Ex3");
        manipularVisibilidade(bEx4, "Ex4");
        manipularVisibilidade(bEx5, "Ex5");
    }

    public static void verificar() {
        bEx1 = downloadManager.verify("meditacao_1.mp3");
        bEx2 = downloadManager.verify("meditacao_2.mp3");

        if (!globalMethods.FREE) {
            bEx3 = downloadManager.verify("meditacao_3.mp3");
            bEx4 = downloadManager.verify("meditacao_4.mp3");
            bEx5 = downloadManager.verify("meditacao_5.mp3");
        }
    }

    @Override
    public void onBackPressed() {
        voltar();
    }

    public void voltar() {
        if(globalMethods.FREE)
            globalMethods.mostrarIntersticial();

        if(bPlaying || bDownloading) {
            intent = new Intent(app_meditacao.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }else
            finish();
    }

    @Override
    protected void onStop() {
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        if (!taskInfo.isEmpty()) {
            ComponentName topActivity = taskInfo.get(0).topActivity;

            if (!topActivity.getPackageName().toString().equals(context.getPackageName())) {
                finishAffinity();
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
            }
        }
        super.onStop();
    }

    public static void setarFuncaoBotoes() {
        final Locale locale = Locale.getDefault();

        final long lEx1;
        final long lEx2;
        final long lEx3;
        final long lEx4;
        final long lEx5;

        if(locale.getLanguage().equals("pt")) {
            lEx1 = 360 * 1000;
            lEx2 = 429 * 1000;
            lEx3 = 364 * 1000;
            lEx4 = 438 * 1000;
            lEx5 = 469 * 1000;
        } else{
            lEx1 = 500 * 1000;
            lEx2 = 243 * 1000;
            lEx3 = 397 * 1000;
            lEx4 = 382 * 1000;
            lEx5 = 361 * 1000;
        }

        if (bEx1 != null && bEx1) {
            btnEx1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (btnEx1.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putInt("audio", 1);
                        //firebaseAnalytics.logEvent("meditacao_start", bundleFirebaseAnalitics);

                        globalMethods.audioReset();
                        btnEx1.setChecked(true);

                        volume = ((float) sbVolume.getProgress() / 10);

                        globalMethods.play("meditacao_1.mp3", volume);

                        bPlaying = true;

                        contDownTimer = new CountDownTimer( lEx1, 1000) {
                            public void onTick(long millisUntilFinished) {
                                txtContadorEx1.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                            }

                            public void onFinish() {
                                btnEx1.setChecked(false);
                                txtContadorEx1.setText("06:00");
                            }

                            public String getCorrectTime(boolean isMinute, long millisUntilFinished) {
                                String aux;
                                int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                                Calendar c = Calendar.getInstance();
                                c.setTimeInMillis(millisUntilFinished);

                                aux = c.get(constCalendar) < 10 ? "0" + c.get(constCalendar) : "" + c.get(constCalendar);

                                return (aux);
                            }
                        }.start();
                    } else {
                        globalMethods.stop();

                        bPlaying = false;

                        if(contDownTimer != null)
                            contDownTimer.cancel();

                        if(locale.getLanguage().equals("pt"))
                            txtContadorEx1.setText("06:00");
                        else
                            txtContadorEx1.setText("08:20");
                    }
                }
            });
        }

        if (bEx2 != null && bEx2) {
            btnEx2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (btnEx2.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putInt("audio", 2);
                        //firebaseAnalytics.logEvent("meditacao_start", bundleFirebaseAnalitics);

                        globalMethods.audioReset();
                        btnEx2.setChecked(true);

                        volume = ((float) sbVolume.getProgress() / 10);

                        globalMethods.play("meditacao_2.mp3", volume);

                        bPlaying = true;

                        contDownTimer = new CountDownTimer(lEx2, 1000) {
                            public void onTick(long millisUntilFinished) {
                                txtContadorEx2.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                            }

                            public void onFinish() {
                                btnEx2.setChecked(false);
                                txtContadorEx2.setText("07:09");
                            }

                            public String getCorrectTime(boolean isMinute, long millisUntilFinished) {
                                String aux;
                                int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                                Calendar c = Calendar.getInstance();
                                c.setTimeInMillis(millisUntilFinished);

                                aux = c.get(constCalendar) < 10 ? "0" + c.get(constCalendar) : "" + c.get(constCalendar);

                                return (aux);
                            }
                        }.start();
                    } else {
                        globalMethods.stop();

                        bPlaying = false;

                        if(contDownTimer != null)
                            contDownTimer.cancel();

                        if(locale.getLanguage().equals("pt"))
                            txtContadorEx2.setText("07:09");
                        else
                            txtContadorEx2.setText("04:03");
                    }
                }
            });
        }

        if (bEx3 != null && bEx3) {
            btnEx3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (btnEx3.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putInt("audio", 3);
                        //firebaseAnalytics.logEvent("meditacao_start", bundleFirebaseAnalitics);

                        globalMethods.audioReset();
                        btnEx3.setChecked(true);

                        volume = ((float) sbVolume.getProgress() / 10);

                        globalMethods.play("meditacao_3.mp3", volume);

                        bPlaying = true;

                        contDownTimer = new CountDownTimer(lEx3, 1000) {
                            public void onTick(long millisUntilFinished) {
                                txtContadorEx3.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                            }

                            public void onFinish() {
                                btnEx3.setChecked(false);
                                txtContadorEx3.setText("06:04");
                            }

                            public String getCorrectTime(boolean isMinute, long millisUntilFinished) {
                                String aux;
                                int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                                Calendar c = Calendar.getInstance();
                                c.setTimeInMillis(millisUntilFinished);

                                aux = c.get(constCalendar) < 10 ? "0" + c.get(constCalendar) : "" + c.get(constCalendar);

                                return (aux);
                            }
                        }.start();
                    } else {
                        globalMethods.stop();

                        bPlaying = false;

                        if(contDownTimer != null)
                            contDownTimer.cancel();

                        if(locale.getLanguage().equals("pt"))
                            txtContadorEx3.setText("06:04");
                        else
                            txtContadorEx3.setText("06:37");
                    }
                }
            });
        }

        if (bEx4 != null && bEx4) {
            btnEx4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (btnEx4.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putInt("audio", 4);
                        //firebaseAnalytics.logEvent("meditacao_start", bundleFirebaseAnalitics);

                        globalMethods.audioReset();
                        btnEx4.setChecked(true);

                        volume = ((float) sbVolume.getProgress() / 10);

                        globalMethods.play("meditacao_4.mp3", volume);

                        bPlaying = true;

                        contDownTimer = new CountDownTimer(lEx4, 1000) {
                            public void onTick(long millisUntilFinished) {
                                txtContadorEx4.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                            }

                            public void onFinish() {
                                btnEx4.setChecked(false);
                                txtContadorEx4.setText("07:18");
                            }

                            public String getCorrectTime(boolean isMinute, long millisUntilFinished) {
                                String aux;
                                int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                                Calendar c = Calendar.getInstance();
                                c.setTimeInMillis(millisUntilFinished);

                                aux = c.get(constCalendar) < 10 ? "0" + c.get(constCalendar) : "" + c.get(constCalendar);

                                return (aux);
                            }
                        }.start();
                    } else {
                        globalMethods.stop();

                        bPlaying = false;

                        if(contDownTimer != null)
                            contDownTimer.cancel();

                        if(locale.getLanguage().equals("pt"))
                            txtContadorEx4.setText("07:18");
                        else
                            txtContadorEx4.setText("06:22");
                    }
                }
            });
        }

        if (bEx5 != null && bEx5) {
            btnEx5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (btnEx5.isChecked()) {
                        //bundleFirebaseAnalitics.clear();
                        //bundleFirebaseAnalitics.putInt("audio", 5);
                        //firebaseAnalytics.logEvent("meditacao_start", bundleFirebaseAnalitics);

                        globalMethods.audioReset();
                        btnEx5.setChecked(true);

                        volume = ((float) sbVolume.getProgress() / 10);

                        globalMethods.play("meditacao_5.mp3", volume);

                        bPlaying = true;

                        contDownTimer = new CountDownTimer(lEx5, 1000) {
                            public void onTick(long millisUntilFinished) {
                                txtContadorEx5.setText(getCorrectTime(true, millisUntilFinished) + ":" + getCorrectTime(false, millisUntilFinished));
                            }

                            public void onFinish() {
                                btnEx5.setChecked(false);
                                txtContadorEx5.setText("07:49");
                            }

                            public String getCorrectTime(boolean isMinute, long millisUntilFinished) {
                                String aux;
                                int constCalendar = isMinute ? Calendar.MINUTE : Calendar.SECOND;

                                Calendar c = Calendar.getInstance();
                                c.setTimeInMillis(millisUntilFinished);

                                aux = c.get(constCalendar) < 10 ? "0" + c.get(constCalendar) : "" + c.get(constCalendar);

                                return (aux);
                            }
                        }.start();
                    } else {
                        globalMethods.stop();

                        bPlaying = false;

                        if(contDownTimer != null)
                            contDownTimer.cancel();

                        if(locale.getLanguage().equals("pt"))
                            txtContadorEx5.setText("07:49");
                        else
                            txtContadorEx5.setText("06:01");
                    }
                }
            });
        }
    }

    public static void manipularVisibilidade(Boolean visibilidade, String elemento) {
        switch (elemento) {
            case "Ex1":
                if (visibilidade == null) {
                    btnEx1.setVisibility(View.GONE);
                    llDescEx1.setVisibility(View.GONE);
                    llManageEx1_2.setVisibility(View.VISIBLE);
                    llManageEx1.setVisibility(View.GONE);
                    llManageEx1_3.setVisibility(View.GONE);
                    bEx1 = true;
                } else if (visibilidade) {
                    btnEx1.setVisibility(View.VISIBLE);
                    llDescEx1.setVisibility(View.VISIBLE);
                    llManageEx1_3.setVisibility(View.VISIBLE);
                    llManageEx1.setVisibility(View.GONE);
                    llManageEx1_2.setVisibility(View.GONE);
                } else if (!visibilidade) {
                    btnEx1.setVisibility(View.GONE);
                    llManageEx1.setVisibility(View.VISIBLE);
                    llDescEx1.setVisibility(View.VISIBLE);
                    llManageEx1_3.setVisibility(View.GONE);
                    llManageEx1_2.setVisibility(View.GONE);
                }
                break;
            case "Ex2":
                if (visibilidade == null) {
                    btnEx2.setVisibility(View.GONE);
                    llDescEx2.setVisibility(View.GONE);
                    llManageEx2_2.setVisibility(View.VISIBLE);
                    llManageEx2.setVisibility(View.GONE);
                    llManageEx2_3.setVisibility(View.GONE);
                    bEx2 = true;
                } else if (visibilidade) {
                    btnEx2.setVisibility(View.VISIBLE);
                    llDescEx2.setVisibility(View.VISIBLE);
                    llManageEx2_3.setVisibility(View.VISIBLE);
                    llManageEx2.setVisibility(View.GONE);
                    llManageEx2_2.setVisibility(View.GONE);
                } else if (!visibilidade) {
                    btnEx2.setVisibility(View.GONE);
                    llManageEx2.setVisibility(View.VISIBLE);
                    llDescEx2.setVisibility(View.VISIBLE);
                    llManageEx2_3.setVisibility(View.GONE);
                    llManageEx2_2.setVisibility(View.GONE);
                }
                break;
            case "Ex3":
                if (visibilidade == null) {
                    btnEx3.setVisibility(View.GONE);
                    llDescEx3.setVisibility(View.GONE);
                    llManageEx3_2.setVisibility(View.VISIBLE);
                    llManageEx3.setVisibility(View.GONE);
                    llManageEx3_3.setVisibility(View.GONE);
                    bEx3 = true;
                } else if (visibilidade) {
                    btnEx3.setVisibility(View.VISIBLE);
                    llDescEx3.setVisibility(View.VISIBLE);
                    llManageEx3_3.setVisibility(View.VISIBLE);
                    llManageEx3.setVisibility(View.GONE);
                    llManageEx3_2.setVisibility(View.GONE);
                } else if (!visibilidade) {
                    btnEx3.setVisibility(View.GONE);
                    llManageEx3.setVisibility(View.VISIBLE);
                    llDescEx3.setVisibility(View.VISIBLE);
                    llManageEx3_3.setVisibility(View.GONE);
                    llManageEx3_2.setVisibility(View.GONE);
                }
                break;
            case "Ex4":
                if (visibilidade == null) {
                    btnEx4.setVisibility(View.GONE);
                    llDescEx4.setVisibility(View.GONE);
                    llManageEx4_2.setVisibility(View.VISIBLE);
                    llManageEx4.setVisibility(View.GONE);
                    llManageEx4_3.setVisibility(View.GONE);
                    bEx4 = true;
                } else if (visibilidade) {
                    btnEx4.setVisibility(View.VISIBLE);
                    llDescEx4.setVisibility(View.VISIBLE);
                    llManageEx4_3.setVisibility(View.VISIBLE);
                    llManageEx4.setVisibility(View.GONE);
                    llManageEx4_2.setVisibility(View.GONE);
                } else if (!visibilidade) {
                    btnEx4.setVisibility(View.GONE);
                    llManageEx4.setVisibility(View.VISIBLE);
                    llDescEx4.setVisibility(View.VISIBLE);
                    llManageEx4_3.setVisibility(View.GONE);
                    llManageEx4_2.setVisibility(View.GONE);
                }
                break;
            case "Ex5":
                if (visibilidade == null) {
                    btnEx5.setVisibility(View.GONE);
                    llDescEx5.setVisibility(View.GONE);
                    llManageEx5_2.setVisibility(View.VISIBLE);
                    llManageEx5.setVisibility(View.GONE);
                    llManageEx5_3.setVisibility(View.GONE);
                    bEx5 = true;
                } else if (visibilidade) {
                    btnEx5.setVisibility(View.VISIBLE);
                    llDescEx5.setVisibility(View.VISIBLE);
                    llManageEx5_3.setVisibility(View.VISIBLE);
                    llManageEx5.setVisibility(View.GONE);
                    llManageEx5_2.setVisibility(View.GONE);
                } else if (!visibilidade) {
                    btnEx5.setVisibility(View.GONE);
                    llManageEx5.setVisibility(View.VISIBLE);
                    llDescEx5.setVisibility(View.VISIBLE);
                    llManageEx5_3.setVisibility(View.GONE);
                    llManageEx5_2.setVisibility(View.GONE);
                }
                break;
        }
    }

    public void manipularArquivo(View view) {
        try {
             if (bEx1 != null) {
                if ((view == llManageEx1_3) && (bEx1)) {
                    manipularVisibilidade(false, "Ex1");
                    bEx1 = !downloadManager.delete("meditacao_1.mp3");
                } else if ((view == llManageEx1) && (!bEx1)) {
                    manipularVisibilidade(null, "Ex1");
                    downloadManager = new DownloadManager(this, this, pbEx1);
                    bEx1 = null;
                    downloadManager.execute("meditacao_1", "Ex1");
                    bDownloading = true;
                }
            }

            if (bEx2 != null) {
                if ((view == llManageEx2_3) && (bEx2)) {
                    manipularVisibilidade(false, "Ex2");
                    bEx2 = !downloadManager.delete("meditacao_2.mp3");
                } else if ((view == llManageEx2) && (!bEx2)) {
                    manipularVisibilidade(null, "Ex2");
                    downloadManager = new DownloadManager(this, this, pbEx2);
                    bEx2 = null;
                    downloadManager.execute("meditacao_2", "Ex2");
                    bDownloading = true;
                }
            }

            if (bEx3 != null) {
                if ((view == llManageEx3_3) && (bEx3)) {
                    manipularVisibilidade(false, "Ex3");
                    bEx3 = !downloadManager.delete("meditacao_3.mp3");
                } else if ((view == llManageEx3) && (!bEx3)) {
                    manipularVisibilidade(null, "Ex3");
                    downloadManager = new DownloadManager(this, this, pbEx3);
                    bEx3 = null;
                    downloadManager.execute("meditacao_3", "Ex3");
                    bDownloading = true;
                }
            }


            if (bEx4 != null) {
                if ((view == llManageEx4_3) && (bEx4)) {
                    manipularVisibilidade(false, "Ex4");
                    bEx4 = !downloadManager.delete("meditacao_4.mp3");
                } else if ((view == llManageEx4) && (!bEx4)) {
                    manipularVisibilidade(null, "Ex4");
                    downloadManager = new DownloadManager(this, this, pbEx4);
                    bEx4 = null;
                    downloadManager.execute("meditacao_4", "Ex4");
                    bDownloading = true;
                }
            }

            if (bEx5 != null) {
                if ((view == llManageEx5_3) && (bEx5)) {
                    manipularVisibilidade(false, "Ex5");
                    bEx5 = !downloadManager.delete("meditacao_5.mp3");
                } else if ((view == llManageEx5) && (!bEx5)) {
                    manipularVisibilidade(null, "Ex5");
                    downloadManager = new DownloadManager(this, this, pbEx5);
                    bEx5 = null;
                    downloadManager.execute("meditacao_5", "Ex5");
                    bDownloading = true;
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, this.getString(R.string.erro_download), Toast.LENGTH_SHORT).show();
        }
    }
}